//
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/wait.h>

void print_estado_hijo(int pid) {
	char cmd[256];
	sprintf(cmd, "ps | grep %d", pid);
	system(cmd);
}
int main(int argc, char *argv[]) {
	int s;

	printf("Inicio padre: PID %d\n", getpid());
	if ((s=fork())<0) {
		perror("error creando proceso hijo");
		return 1;
	}
	if (s==0) {
		printf("Inicio hijo: PID %d y PID del padre %d\n", getpid(), getppid());
		printf("Fin hijo: PID %d y PID del padre %d\n", getpid(), getppid());
		return 0;
	}
	sleep(2);

	print_estado_hijo(s);
	wait(NULL);
	printf("Fin padre: PID %d\n", getpid());
	return 0;
}

