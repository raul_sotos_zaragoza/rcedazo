// El proceso hijo se queda huérfano y es adoptado
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>

int main(int argc, char *argv[]) {
	int s;

	printf("Inicio padre: PID %d\n", getpid());
	if ((s=fork())<0) {
		perror("error creando proceso hijo");
		return 1;
	}
	if (s==0) {
		printf("Inicio hijo: PID %d y PID del padre %d\n", getpid(), getppid());
		sleep(10);
		printf("Fin hijo: PID %d y PID del padre %d\n", getpid(), getppid());
		return 0;
	}
	sleep(1);
	printf("Fin padre: PID %d\n", getpid());
	return 0;
}

