// Este ejemplo muestra el
// de acceso a los argumentos y a una variable de entorno
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
	// imprime varios IDs
	printf("PID %d PPID %d\n", getpid(), getppid());
	printf("UID %d GID %d\n", getuid(), getgid());
	printf("EUID %d EGID %d\n", geteuid(), getegid());

	// Si este ejecutable pertenece al root (./cambia_a_root)
	// y tiene activos SETUID y SETGID (./cambia_a_root)
	// cambiarán el UID y GID efectivos
	
	// ¿tengo superpoderes?
	int f;
	if ((f=open("/etc/shadow", O_RDWR))<0)
		printf("no tengo superpoderes\n");
	else {
		printf("¡tengo superpoderes!\n");
		close(f);

		// revierto los IDs efectivos a los reales
		seteuid(getuid());
		setegid(getgid());
		printf("después de seteuid: UID %d GID %d\n", getuid(), getgid());
		printf("después de setegid: EUID %d EGID %d\n", geteuid(), getegid());
		// ¿sigo teniendo superpoderes?
		if ((f=open("/etc/shadow", O_RDWR))<0)
			printf("ya no tengo superpoderes\n");
		else {
			printf("¡sigo teniendo superpoderes!\n");
			close(f);
		}
	}
	return 0;
}
