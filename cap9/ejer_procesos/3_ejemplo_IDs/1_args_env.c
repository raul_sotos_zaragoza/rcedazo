// Este ejemplo muestra el
// de acceso a los argumentos y a variables de entorno
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
	int i;
	char *val;

	// imprime todos los argumentos
	for (i=0; i<argc; i++)
		printf("argv[%d] = %s\n", i, argv[i]);

	// accede a variables de entorno 
	if ((val = getenv("MIVAR")) != NULL) 
		printf("MIVAR = %s\n", val);
	else
		printf("Variable de entorno MIVAR no definida\n");

	if ((val = getenv("LANG")) != NULL) 
		printf("LANG = %s\n", val);
	else
		printf("Variable de entorno LANG no definida\n");

	// ejecuto ls de un fichero que no existe:
	// el mensaje debe salir en el idioma definido en la variable LANG
	system("ls NOEXISTE");

	// cambio el idioma en la variable LANG
	putenv("LANG=en_US.utf8");
	if ((val = getenv("LANG")) != NULL) 
		printf("LANG = %s\n", val);
	else
		printf("Variable de entorno LANG no definida\n");

	// ahora el mensaje debe salir en inglés
	system("ls NOEXISTE");

	return 0;
}
