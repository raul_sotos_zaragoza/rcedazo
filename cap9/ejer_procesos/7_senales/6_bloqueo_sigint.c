// ilustra cómo se puede bloquear temporalmente la entrega de una señal usando
// el servicio sigprocmask y cómo bloquearla durante el tratamiento de otra señal.
#include <stdio.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>

void trata_sigint(int s) {
	printf("Función tratamiento de señal SIGINT\n");
}
void trata_alarma(int s) {
	printf("Inicio función tratamiento de señal SIGALRM\n");
	printf("Pulsa Crtl-C: tratamiento de SIGINT esperará a fin de tratamiento de SIGALRM\n");
	sleep(5);
	printf("Fin función tratamiento de señal SIGALRM\n");
	
}
int main(int argc, char *argv[]) {
	sigset_t mascara, mascara_previa;
	struct sigaction acc;

	printf("La señal SIGINT está capturada\n");
        acc.sa_handler = trata_sigint;
        acc.sa_flags=0;
        sigemptyset(&acc.sa_mask);
        sigaction(SIGINT, &acc, NULL);

	printf("Fase 1: Bloqueo de señal mediante sigprocmask\n");
	sigemptyset(&mascara);
	sigaddset(&mascara, SIGINT);
	sigprocmask(SIG_BLOCK, &mascara, &mascara_previa);
	printf("Bloqueo de SIGINT mediante sigprocmask\n");
	printf("El proceso se va a dormir 5 segundos y luego desbloqueará la señal\n");
	printf("Mientras duerme pulsa Crtl-C: tratamiento esperará a fin de bloqueo cuando se despierte\n");
	sleep(5);
	printf("Proceso de despierta: desbloqueo de SIGINT mediante sigprocmask\n");
	sigprocmask(SIG_SETMASK, &mascara_previa, NULL);

	printf("\nFase 2: Bloqueo de señal durante tratamiento de otra señal\n");
	printf("Captura de SIGALRM con sigaction bloqueando SIGINT\n");
	acc.sa_handler = trata_alarma;
	acc.sa_flags=0;
	sigemptyset(&acc.sa_mask);
	sigaddset(&acc.sa_mask, SIGINT);
	sigaction(SIGALRM, &acc, NULL);
	printf("Arranca alarma de 1 segundo\n");
	alarm(1);
	pause();

	return 0;
}
