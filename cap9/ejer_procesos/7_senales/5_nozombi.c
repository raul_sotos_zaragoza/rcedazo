// Ejemplo que muestra cómo evitar que un proceso pase al estado zombi
// ignorando la señal SIGCHLD.

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>


int main(int argc, char *argv[]) {
	int rf;
	char cmd[32];
        struct sigaction acc;

        acc.sa_handler = SIG_IGN;
        acc.sa_flags=0;
        sigemptyset(&acc.sa_mask);
        sigaction(SIGCHLD, &acc, NULL);
	if ((rf = fork()) == -1) {
		perror("fork");
		return 1;
	}
	else if (rf == 0) {
		printf("HIJO TERMINA\n");
		return 0;
	}
	printf("PADRE DUERME 3 SEGUNDOS PARA QUE HIJO %d PUEDA TERMINAR\n", rf);
	sleep(3);
	printf("PADRE DESPIERTA Y EJECUTA 'ps' PARA VER ESTADO DEL HIJO:\n"); 
	sprintf(cmd, "ps %d", rf);
	system(cmd);

	return 0;
}
