/* Programa que temporiza a su hijo y le mata si no termina en argv[1] segundos.
   Prueba a comentar y descomentar SA_RESTART */
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/wait.h>
#include <unistd.h>

pid_t pid;
void tratar_alarma (int s){
	kill (pid, SIGKILL);
}

int main (int argc, char * argv[]){
	if (argc<3) {
		fprintf(stderr, "Uso: %s plazo programa args...\n", argv[0]);
		return 1;
	}
	int  status;
	struct sigaction act;
 	char ** argumentos = &argv[2];
	switch (pid = fork ()) {
 		case - 1 : perror("fork"); exit(1);
		case   0 : /* proceso hijo */
			execvp(argumentos [0], argumentos);
			perror("exec"); exit(1);
		default: /* padre */
			/* establece el manejador */
			act.sa_handler = tratar_alarma; /* función a ejecutar */
			sigemptyset(&act.sa_mask); /* No bloquea ninguna señal */
			act.sa_flags = SA_RESTART ; /* evita E_INTR en wait */
			sigaction(SIGALRM, &act, NULL);
			alarm(atoi(argv[1]));
			if (wait(&status)<0) {
				perror("wait");
				exit(1);
			}
			if (WIFEXITED(status))
				printf("hijo completado\n");
			if (WIFSIGNALED(status))
				printf("hijo abortado\n");
	}
	return 0;
}
