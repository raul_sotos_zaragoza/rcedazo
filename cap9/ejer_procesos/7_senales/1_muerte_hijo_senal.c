// Ejemplo que muestra el estado que recoge wait dependiendo de cómo muera el hijo.

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

int main(int argc, char *argv[]) {
	int rf, tipo_muerte, estado;
	int x=7, y=0;
	char *p = NULL;

	// necesario para tipo de muerte 7: queremos que muera el hijo pero no el padre
        struct sigaction acc;
        acc.sa_handler = SIG_IGN;
        acc.sa_flags=0;
        sigemptyset(&acc.sa_mask);
        sigaction(SIGINT, &acc, NULL);
        sigaction(SIGQUIT, &acc, NULL);

	while(1){
		printf("\nIntroduzca tipo de muerte:\n\t0: div por 0\n\t1: acceso a memoria inválido\n\t2: ejecuta abort()\n\t3: ejecuta alarm(1)\n\t4: proceso en pausa\n\t5: terminar el programa:\n");
		scanf("%d", &tipo_muerte);
		if (tipo_muerte>=5) return 0;
		
		if ((rf = fork()) == -1) {
			perror("fork");
			return 1;
		}
		else if (rf == 0) {
			// necesario para tipo de muerte 7: queremos que muera el hijo pero no el padre
        		acc.sa_handler = SIG_DFL;
        		sigaction(SIGINT, &acc, NULL);
        		sigaction(SIGQUIT, &acc, NULL);

			switch (tipo_muerte) {
				case 0:
					printf("\nHIJO DIVIDE POR 0\n");
					x = x/y;
				case 1:
					printf("\nHIJO ACCESO A MEMORIA INVÁLIDO\n");
					*p=5;
				case 2:
					printf("\nHIJO EJECUTA abort()\n");
					abort();
				case 3:
					printf("\nHIJO EJECUTA alarm(1)\n");
					alarm(1);
					pause();
				case 4:
					printf("\nHIJO EN pause()\n");
					printf("PRUEBA A PULSAR CONTROL-C O CONTROL-\\\n");
					printf("O ENVÍE CUALQUIER SEÑAL DESDE OTRA VENTANA:\n");
					printf("kill -nºsenal %d\n", getpid());
					pause();
			}
		
		}
		if (waitpid(rf, &estado, 0)<0) {
			perror("wait");
			return 1;
		}

		if (WIFEXITED(estado))
			printf("wait: hijo fin normal con valor %d\n",
				WEXITSTATUS(estado));
		if (WIFSIGNALED(estado))
			printf("wait: hijo fin por señal con valor %d y nombre \"%s\"\n",
				WTERMSIG(estado), strsignal(WTERMSIG(estado)));
	}
	return 0;
}
