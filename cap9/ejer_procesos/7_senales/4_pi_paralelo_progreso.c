// Versión paralela del cálculo de PI.
// Periódicamente los procesos hijos van imprimiendo cuánto progreso
// han hecho en su cálculo. Para ello, el padre se despierta
// periódicamente mediante la señal de alarma y envía la señal SIGUSR1
// a los procesos hijos para que impriman su información de progreso.
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/times.h>
#include <sys/wait.h>
#include <math.h>

int i, niter_proc, nproc, nsegs;
int *pids;

void trata_aviso(int s) {
	printf("Proceso %d: completado %f%%\n", getpid(),
			100.0 * i/niter_proc);
}
void trata_alarma(int s) {
	alarm(nsegs);
	for (int p=0; p<nproc; p++) kill(pids[p], SIGUSR1); 
}
int main(int argc, char *argv[]){
        unsigned int n, nhits=0, niter;
	double x, y, pi;
	int tub[2];
        struct sigaction acc;

	if (argc!=4)  {
                fprintf (stderr, "Uso: %s plazo_progreso nºiteraciones nºprocesos\n", argv[0]);
                return 1;
        }

	nsegs=atoi(argv[1]);
	niter=atoi(argv[2]);
	nproc=atoi(argv[3]);
	// cuántas iteraciones hará cada proceso (redondeando por exceso)
	niter_proc = (niter + nproc -1)/nproc;

	// calculo una semilla aleatoria
	srand(getpid()*times(NULL));

	// creo una tubería
	pipe(tub);

	pids = malloc(nproc * sizeof(int));

	for (int p=0; p<nproc; p++) {
		if ((pids[p]=fork())==0) {
        		acc.sa_handler = trata_aviso;
        		acc.sa_flags=0;
        		sigemptyset(&acc.sa_mask);
        		sigaction(SIGUSR1, &acc, NULL);

			close(tub[0]);
			for (i=0; i<niter_proc; i++) {
				// genero un punto formado por dos aleatorios en el rango [0,2]
				x = 2.0 * rand()/RAND_MAX;
				y = 2.0 * rand()/RAND_MAX;
				// compruebo si el punto está dentro de un círculo de radio 1
				// con centro en (1,1)
				if (((x-1)*(x-1) + (y-1)*(y-1)) <=1)
                    		nhits++;
			}
			write(tub[1], &nhits, sizeof(nhits));
			close(tub[1]);
			return 0;
		}
	}
        acc.sa_handler = trata_alarma;
        acc.sa_flags=SA_RESTART;
        sigemptyset(&acc.sa_mask);
        sigaction(SIGALRM, &acc, NULL);
	alarm(nsegs);

	close(tub[1]);
	for (int p=0; p<nproc; p++) {
		read(tub[0], &n, sizeof(n));
		nhits+=n;
		wait(NULL);
	}
	close(tub[0]);
	pi = 4.0 * nhits/niter;
	printf("PI estimado %lf PI real %lf\n", pi, M_PI);
	return 0;
}

