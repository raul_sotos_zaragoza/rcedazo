// Ejemplo que muestra el estado que recoge wait dependiendo de cómo
// muera el hijo. Asimismo, imprime cuánto tiempo ha ejecutado el hijo.

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/times.h>

int main(int argc, char *argv[]) {
	int rf, tipo_muerte, estado;
	int x=7, y=0;
	char *p = NULL;
	struct tms t;
	clock_t ci, cf;
        long ticsporseg;

	ticsporseg = sysconf(_SC_CLK_TCK);

	if (argc != 2) {
		fprintf(stderr, "Uso: %s tipo_de_muerte(de 0 a 8)\n", argv[0]);
		return 1;
	}
	tipo_muerte = atoi(argv[1]);
	signal(SIGINT, SIG_IGN); //necesario para tipo de muerte 7

	ci = times(NULL);
	if ((rf = fork()) == -1) {
		perror("fork");
		return 1;
	}
	else if (rf == 0) {
		signal(SIGINT, SIG_DFL); //necesario para tipo de muerte 7
		
		// el hijo ejecuta un cierto código
		for (int i=0, j=0; i<1000000; i++, j+=getpid());
		sleep(1);

		switch (tipo_muerte) {
			case 0:
				printf("HIJO FIN NORMAL CON 0\n");
				return 0;
			case 1:
				printf("HIJO FIN NORMAL CON 1\n");
				return 1;
			case 2:
				printf("HIJO FIN NORMAL CON 1028 (VALOR DEMASIADO GRANDE)\n");
				return 1028;
			case 3:
				printf("HIJO DIVIDE POR 0\n");
				x = x/y;
			case 4:
				printf("HIJO ACCESO A MEMORIA INVÁLIDO\n");
				*p=5;
			case 5:
				printf("HIJO SE AUTOENVÍA SIGUSR1\n");
				kill(getpid(), SIGUSR1);
			case 6:
				printf("HIJO SE AUTOENVÍA SEÑAL DE ACCESO A MEMORIA INVÁLIDO\n");
				kill(getpid(), SIGSEGV);
			case 7:
				printf("HIJO MUERE POR CONTROL-C\n");
				pause();
			case 8:
				printf("HIJO MUERE POR SEÑAL ENVIADA POR OTRO PROCESO\n");
				printf("EJECUTE DESDE OTRA VENTANA:\n");
				printf("kill %d\n", getpid());
				pause();
			default:
				printf("TIPO DE MUERTE NO IMPLEMENTADA\n");
				return 0;
			}
		
	}
	if (waitpid(rf, &estado, 0)<0) {
		perror("wait");
		return 1;
	}
	cf = times(&t);

	if (WIFEXITED(estado))
		printf("wait: hijo fin normal con valor %d\n",
			WEXITSTATUS(estado));
	if (WIFSIGNALED(estado))
		printf("wait: hijo fin por señal con valor %d\n",
			WTERMSIG(estado));
        fprintf(stderr, "real %lf usuario %lf sistema %lf\n",
                (cf - ci)/(double)ticsporseg,
                t.tms_cutime/(double)ticsporseg,
                t.tms_cstime/(double)ticsporseg);
	return 0;
}
