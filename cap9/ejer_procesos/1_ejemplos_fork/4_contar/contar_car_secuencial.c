/* Cuenta cuántas veces aparece un carácter en un fichero usando read/write.
   Versión secuencial */
#include <sys/types.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <ctype.h>

int main(int  argc, char *argv[]) {
	int f, n, contador=0;
	unsigned char caracter;
        unsigned char buf[4096];

        if (argc!=3)
        {
                fprintf (stderr, "Uso: %s caracter fichero\n", argv[0]);
                return(1);
        }

        /* Por simplificar, se supone que el carácter a contar corresponde
           con el primero del primer argumento */
        caracter=argv[1][0];

        /* Abre el fichero */
        if ((f=open(argv[2], O_RDONLY))<0)
        {
		perror("No puede abrirse el archivo");
		return(1);
	}

        /* Bucle de lectura del fichero */
	while ((n = read(f, buf, sizeof(buf)))>0) {
		for (int i=0; i<n; i++) {
			if (buf[i]==caracter) contador++;
		}
	}
	close(f);
	printf("%d\n", contador);

	return 0;
}
