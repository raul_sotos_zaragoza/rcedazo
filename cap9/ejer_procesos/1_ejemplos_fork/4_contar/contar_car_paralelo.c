/* Cuenta cuántas veces aparece un carácter en un fichero usando read/write.
   Versión paralela */
#include <sys/wait.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int  argc, char *argv[]) {
	int f, n, nproc, contador=0;
	unsigned char caracter;
        unsigned char buf[4096];
	int tub[2];

        if (argc!=4) {
                fprintf (stderr, "Uso: %s caracter fichero nºprocesos\n", argv[0]);
                return 1;
        }

        /* Por simplificar, se supone que el carácter a contar corresponde
           con el primero del primer argumento */
        caracter=argv[1][0];

        nproc=atoi(argv[3]);

        /* Abre el fichero */
        if ((f=open(argv[2], O_RDONLY))<0) {
		perror("No puede abrirse el archivo");
		return(1);
	}

	// creo una tubería
        pipe(tub);

	// bucle de creación de procesos
        for (int p=0; p<nproc; p++) {
                if (fork()==0) {
                        close(tub[0]);

        		/* Bucle de lectura del fichero */
			while ((n = read(f, buf, sizeof(buf)))>0) {
				for (int i=0; i<n; i++) {
					if (buf[i]==caracter) contador++;
				}
			}
			close(f);
                        write(tub[1], &contador, sizeof(contador));
                        close(tub[1]);
                        return 0;
                }

	}
	close(f);
        close(tub[1]);
        for (int p=0; p<nproc; p++) {
                read(tub[0], &n, sizeof(n));
                contador+=n;
                wait(NULL);
        }
        close(tub[0]);

	printf("%d\n", contador);

	return 0;
}
