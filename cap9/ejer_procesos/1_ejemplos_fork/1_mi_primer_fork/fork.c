// Muestra cómo después de un fork aparece un nuevo proceso hijo que es
// un duplicado del previo y que:
// 	- accede a los valores de las variables que ha escrito el padre
// 	- puede usar los descriptores para acceder a los ficheros que ha
// 	  abierto el padre compartiendo el puntero de posición
// 	- recibe como valor de retorno de fork un 0, mientras que el padre
// 	  recibe el PID del hijo
// Pero tal que después del fork los cambios que hace el padre no los
// ve el hijo ni viceversa:
// 	- los dos procesos actualizan la variable pero cada un solo ve su
// 	  actualización
// 	- si cada proceso abre el mismo fichero no comparten el puntero
//
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/wait.h>

int main(int argc, char *argv[]) {
	int s, f1, f2, contador;
pause();
	contador=666;
	if ((f1=open("TMP1", O_CREAT|O_WRONLY, 0640))<0) {
		perror("error creando fichero temporal");
		return 1;
	}
	printf("Antes de fork: Mi PID %d y el PID de mi padre %d\n", getpid(), getppid());
	if ((s=fork())<0) {
		perror("error creando proceso hijo");
		return 1;
	}
	contador++;
	if ((f2=open("TMP2", O_CREAT|O_WRONLY, 0640))<0) {
		perror("error creando fichero temporal");
		return 1;
	}
	if (write(f1, "hola1", 5)<0) {
		perror("error escribiendo en fichero TMP1");
		return 1;
	}
	if (write(f2, "hola2", 5)<0) {
		perror("error escribiendo en fichero TMP2");
		return 1;
	}
	close(f1);
	close(f2);

	printf("Después de fork: Mi PID %d y el PID de mi padre %d; valor retornado por fork %d; contador %d\n", getpid(), getppid(), s, contador);

	// el padre espera a que termine el hijo e imprime el contenido
	// de los ficheros TMP1 y TMP2
	if (s!=0) {
		wait(NULL);
		printf("------------------  TMP1 -------------------\n");
		system("cat TMP1");
		printf("\n--------------------------------------------\n");
		printf("------------------  TMP2 -------------------\n");
		system("cat TMP2");
		printf("\n--------------------------------------------\n");
	}
	return 0;
}

