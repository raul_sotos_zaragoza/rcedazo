/* Cuenta cu�ntas veces aparece un car�cter en un fichero usando read/write.
   Versi�n paralela con open y mmap en el hijo */
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int  argc, char *argv[]) {
	int i, f, n, nproc, contador=0;
	int ncar_proceso, ini, fin;
	char caracter;
	char *v;
	struct stat bstat;
	int tub[2];

        if (argc!=4) {
                fprintf (stderr, "Uso: %s caracter fichero n�procesos\n", argv[0]);
                return 1;
        }

	/* Por simplificar, se supone que el car�cter a contar corresponde
	   con el primero del primer argumento */
	caracter=argv[1][0];

        nproc=atoi(argv[3]);

	/* Averigua la longitud del fichero */
	if (stat(argv[2], &bstat)) {
		perror("Error en fstat del fichero");
		return 1;
	}

        // �cu�ntos caracteres le toca a cada proceso?
        ncar_proceso = bstat.st_size/nproc;

        // creo una tuber�a
        pipe(tub);

        // bucle de creaci�n de procesos
        for (int p=0; p<nproc; p++) {
                if (fork()==0) {
                        close(tub[0]);

			/* Abre el fichero para lectura */
			if ((f=open(argv[2], O_RDONLY))<0) {
				perror("No puede abrirse el fichero");
				return 1;
			}
			/* Se proyecta todo el fichero */
			if ((v=mmap((caddr_t) 0, bstat.st_size, PROT_READ, MAP_SHARED, f, 0))
						==MAP_FAILED) {
				perror("Error en la proyeccion del fichero");
				close(f);
				return 1;
			}
			/* Se cierra el fichero */
			close(f);

			ini=p*ncar_proceso;

			// el �ltimo se lleva el resto
                        if (p==nproc-1)
                                ncar_proceso+=bstat.st_size%nproc;

			fin=ini+ncar_proceso;

			/* Bucle de acceso */
			for (i=ini; i<fin; i++)
				if (v[i]==caracter) contador++;
			write(tub[1], &contador, sizeof(contador));
                        close(tub[1]);
			/* Se eliminan las proyecciones */
			munmap(v, bstat.st_size);
                        return 0;
		}
	}

        close(tub[1]);
        for (int p=0; p<nproc; p++) {
                read(tub[0], &n, sizeof(n));
                contador+=n;
                wait(NULL);
        }
        close(tub[0]);

	printf("%d\n", contador);
	return 0;
}

