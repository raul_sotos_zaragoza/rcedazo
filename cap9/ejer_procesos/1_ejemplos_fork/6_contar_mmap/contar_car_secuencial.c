/* Cuenta cu�ntas veces aparece un car�cter en un fichero usando read/write.
   Versi�n secuencial */
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>

int main(int  argc, char **argv)
{
	int i, fd, contador=0;
	char caracter;
	char *p;
	struct stat bstat;

	if (argc!=3) {
		fprintf (stderr, "Uso: %s caracter fichero\n", argv[0]);
		return 1;
	}

	/* Por simplificar, se supone que el car�cter a contar corresponde
	   con el primero del primer argumento */
	caracter=argv[1][0];

	/* Abre el fichero para lectura */
	if ((fd=open(argv[2], O_RDONLY))<0) {
		perror("No puede abrirse el fichero");
		return 1;
	}

	/* Averigua la longitud del fichero */
	if (fstat(fd, &bstat)) {
		perror("Error en fstat del fichero");
		close(fd);
		return 1;
	}

	/* Se proyecta todo el fichero */
	if ((p=mmap((caddr_t) 0, bstat.st_size, PROT_READ, MAP_SHARED, fd, 0))
				==MAP_FAILED) {
		perror("Error en la proyeccion del fichero");
		close(fd);
		return 1;
	}

	/* Se cierra el fichero */
	close(fd);

	/* Bucle de acceso */
	for (i=0; i<bstat.st_size; i++)
		if (p[i]==caracter) contador++;

	/* Se eliminan las proyecciones */
	munmap(p, bstat.st_size);

	printf("%d\n", contador);
	return 0;
}

