// Versión paralela del cálculo de PI que usa una tubería para resolver el
// problema de que los procesos emparentados no comparten memoria.
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/times.h>
#include <sys/wait.h>
#include <math.h>

int main(int argc, char *argv[]){
        unsigned int n, nhits=0, niter, nproc, niter_proc;
	double x, y, pi;
	int tub[2];

	if (argc!=3)  {
                fprintf (stderr, "Uso: %s nºiteraciones nºprocesos\n", argv[0]);
                return 1;
        }
	niter=atoi(argv[1]);
	nproc=atoi(argv[2]);
	// cuántas iteraciones hará cada proceso (redondeando por exceso)
	niter_proc = (niter + nproc -1)/nproc;

	// calculo una semilla aleatoria
	srand(getpid()*times(NULL));

	// creo una tubería
	pipe(tub);

	for (int p=0; p<nproc; p++) {
		if (fork()==0) {
			close(tub[0]);
			for (int i=0; i<niter_proc; i++) {
				// genero un punto formado por dos aleatorios en el rango [0,2]
				x = 2.0 * rand()/RAND_MAX;
				y = 2.0 * rand()/RAND_MAX;
				// compruebo si el punto está dentro de un círculo de radio 1
				// con centro en (1,1)
				if (((x-1)*(x-1) + (y-1)*(y-1)) <=1)
                    		nhits++;
			}
			write(tub[1], &nhits, sizeof(nhits));
			close(tub[1]);
			return 0;
		}
	}
	close(tub[1]);
	for (int p=0; p<nproc; p++) {
		read(tub[0], &n, sizeof(n));
		nhits+=n;
		wait(NULL);
	}
	close(tub[0]);
	pi = 4.0 * nhits/niter;
	printf("PI estimado %lf PI real %lf\n", pi, M_PI);
	return 0;
}

