// El cálculo de PI usando el método de Montecarlo usa un enfoque
// basado en probabilidades para realizar esta labor tal como se describe en
// el fichero LEEME.html
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/times.h>
#include <math.h>

int main(int argc, char *argv[]){
        unsigned int nhits=0, niter;
	double x, y, pi;

	if (argc!=2)  {
                fprintf (stderr, "Uso: %s nºiteraciones\n", argv[0]);
                return(1);
        }
	niter=atoi(argv[1]);

	// calculo una semilla aleatoria
	srand(getpid()*times(NULL));

	for (int i=0; i<niter; i++) {
		// genero un punto formado por dos aleatorios en el rango [0,2]
		x = 2.0 * rand()/RAND_MAX;
		y = 2.0 * rand()/RAND_MAX;
		// compruebo si el punto está dentro de un círculo de radio 1
		// con centro en (1,1)
		if (((x-1)*(x-1) + (y-1)*(y-1)) <=1)
                    nhits++;
	}
	pi = 4.0 * nhits/niter;
	printf("PI estimado %lf PI real %lf\n", pi, M_PI);
	return 0;
}

