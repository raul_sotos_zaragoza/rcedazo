// Realiza dos fork consecutivos actualizando una variable y escribiendo en un fichero
// sobre el que los ficheros comparten el puntero de posición
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>

int main(int argc, char *argv[]) {
	int x=0;

	int f=creat("TMP", 0600);
	x++;
	write(f, "F", 1);
	fork();
	x++;
	write(f, "F", 1);
	fork();
	x++;
	write(f, "F", 1);
	close(f);
	printf("PID %d x %d\n", getpid(), x);
	return 0;
}
