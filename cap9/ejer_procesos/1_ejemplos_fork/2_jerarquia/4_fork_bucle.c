// Realiza un bucle de llamadas a fork
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
int main(int argc, char *argv[]) {
	int x=0;
	if (argc!=2)  {
                fprintf (stderr, "Uso: %s nºiteraciones\n", argv[0]);
                return(1);
        }
        int niter=atoi(argv[1]);
	int f=creat("TMP", 0600);

	for (int i=0; i<niter; i++) {
		x++;
		write(f, "F", 1);
		fork();
	}
	close(f);
	printf("PID %d x %d\n", getpid(), x);
	return 0;
}
