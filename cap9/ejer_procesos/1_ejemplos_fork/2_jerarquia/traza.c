// NO ES NECESARIO QUE ENTIENDA EL FUNCIONAMIENTO DE ESTE FICHERO.
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

char c;
static int tub1[2];
static int tub2[2];
__attribute__ ((constructor)) void _inicio(void) {
	char cmd[64];
	char c;
	int hijo;
	pipe(tub1);
	pipe(tub2);
	if ((hijo=fork())==0) {
		close(tub1[0]);
		close(tub2[1]);
		return;
	}
	close(tub1[1]);
	close(tub2[0]);
	while (read(tub1[0], &c, 1)>0);
	close(tub1[0]);
	sprintf(cmd, "pstree -p %d", hijo);
	system(cmd);
	close(tub2[1]);
	_exit(0);
}

__attribute__ ((destructor)) void _fin(void) {
	char c;
	write(tub1[1], &c, 1);
	close(tub1[1]);
	read(tub2[0], &c, 1);
	close(tub2[0]);
}
