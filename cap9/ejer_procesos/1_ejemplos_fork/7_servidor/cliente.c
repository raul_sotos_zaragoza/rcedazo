// Envía a la cola de servicio una petición que consiste del
// nombre de fichero cuyo número de líneas quiere conocer (esa labor
// es el servicio: contar las líneas de un fichero) y el nombre de
// una cola donde el cliente espera recibir la respuesta.
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>

int main(int  argc, char *argv[]) {
	int pet, res, nlineas;
	char *cola_respuesta;
        if (argc!=2) {
                fprintf (stderr, "Uso: %s fichero\n", argv[0]);
                return 1;
        }
	// obtiene acceso a la cola de servicio
        if ((pet=open("cola_servicio", O_WRONLY))<0) {
                perror("No puede abrirse la cola de servicio");
                return 1;
        }
	/* crea una cola para poder recibir la respuesta */
	cola_respuesta = tmpnam(NULL);
        if (mkfifo(cola_respuesta, 0600)<0) {
                perror("No puede crearse la cola de respuesta");
                return 1;
        }
	// envía la petición a la cola de servicio:
	// - el nombre del fichero del que queremos saber su nº de líneas
	// - el nombre de la cola de respuesta
	if (dprintf(pet, "%s %s\n", argv[1], cola_respuesta)<=0) {
                perror("No puede escribirse en la cola de servicio");
                return 1;
        }
	// obtiene acceso a la cola de respuesta
        if ((res=open(cola_respuesta, O_RDONLY))<0) {
                perror("No puede abrirse la cola de respuesta");
                return 1;
        }
	// lee la respuesta
	if (read(res, &nlineas, sizeof(int))!=sizeof(int)) {
                perror("No puede escribirse en la cola de servicio");
                return 1;
        }
	close(res);
	unlink(cola_respuesta);
	printf("El fichero %s tiene %d líneas\n", argv[1], nlineas);
        return 0;
}

