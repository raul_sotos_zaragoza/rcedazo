// Recibe por la cola de servicio una petición formada por el
// nombre de un fichero y el de una cola de respuesta, calcula cuántas
// líneas tiene el fichero y envía el resultado a la cola de respuesta.
// Versión secuencial del servidor.
#include <sys/types.h>
#include <sys/mman.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>

static int cuenta_lineas(const char *f) {
        int fd, contador=0;
        char *p;
        struct stat bstat;


        /* Abre el fichero origen para lectura */
        if ((fd=open(f, O_RDONLY))<0) {
                perror("No puede abrirse el fichero pedido");
                return -1;
        }
        /* Averigua la longitud del fichero pedido */
        if (fstat(fd, &bstat)) {
                perror("Error en fstat del fichero pedido");
                close(fd);
                return -1;
        }
        if ((p=mmap((caddr_t) 0, bstat.st_size, PROT_READ, MAP_SHARED, fd, 0))
                                ==MAP_FAILED) {
                perror("Error en la proyeccion del fichero pedido");
                close(fd);
                return -1;
        }
        /* Se cierra el fichero */
        close(fd);

        /* Bucle de acceso */
        for (int i=0; i<bstat.st_size; i++)
                if (p[i]=='\n') contador++;

        munmap(p, bstat.st_size);
        return contador;
}

int main(int  argc, char *argv[]) {
	int res;
	char *fichero, *cola_respuesta;
	FILE *pet;
	

	/* crea la cola para recibir peticiones  */
        unlink("cola_servicio");
        if (mkfifo("cola_servicio", 0600)<0) {
                perror("No puede crearse la cola de peticiones");
                return 1;
	}
	while (1) {
        	if ((pet=fopen("cola_servicio", "r"))==NULL) {
               		perror("No puede abrirse la cola de peticiones");
               		return 1;
        	}
		while (1) {
			// lee una petición
			int n=fscanf(pet, "%ms%ms", &fichero, &cola_respuesta);
			if (n==-1) {
				fclose(pet);
				break;
			}
			printf("inicio servicio %s\n", fichero);
			int nlineas = cuenta_lineas(fichero);
			free(fichero);
			// obtiene acceso a la cola de respuesta
        		if ((res=open(cola_respuesta, O_WRONLY))<0)
                		perror("No puede abrirse la cola de respuesta");
        		
			free(cola_respuesta);
			if (res!=-1) {
				// envía la respuesta
				if (write(res, &nlineas, sizeof(nlineas))<0)
                			perror("No puede abrirse la cola de respuesta");
				close(res);
			}
			printf("fin servicio\n");
		}
	}
        return 0;
}

