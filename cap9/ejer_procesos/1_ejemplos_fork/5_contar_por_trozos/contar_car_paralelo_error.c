/* Cuenta cuántas veces aparece un carácter en un fichero usando read/write.
   Versión paralela errónea */
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int  argc, char *argv[]) {
	int f, n, nproc, contador=0;
	unsigned char caracter;
        unsigned char buf[4096];
	int tub[2];
	struct stat bstat;
	int nbloques_fichero, nbloques_proceso;

        if (argc!=4) {
                fprintf (stderr, "Uso: %s caracter fichero nºprocesos\n", argv[0]);
                return 1;
        }

        /* Por simplificar, se supone que el carácter a contar corresponde
           con el primero del primer argumento */
        caracter=argv[1][0];

        nproc=atoi(argv[3]);

        /* Abre el fichero */
        if ((f=open(argv[2], O_RDONLY))<0) {
		perror("No puede abrirse el archivo");
		return 1;
	}
	/* Averigua la longitud del fichero origen */
        if (fstat(f, &bstat)) {
                perror("Error en fstat del fichero origen");
                close(f);
                return 1;
        }
	// ¿cuántos bloques ocupa el fichero (redondeado por exceso)?
	nbloques_fichero = (bstat.st_size + 4095)/4096;

	// ¿cuántos bloques le toca a cada proceso?
	nbloques_proceso = nbloques_fichero/nproc;

	// creo una tubería
        pipe(tub);

	// bucle de creación de procesos
        for (int p=0; p<nproc; p++) {
                if (fork()==0) {
                        close(tub[0]);
			// se posiciona el puntero en la parte de ese proceso
			lseek(f, p * nbloques_proceso * 4096, SEEK_SET);
			
			// el último se lleva el resto
			int tam=nbloques_proceso;
			if (p==nproc-1)
				tam = nbloques_proceso+nbloques_fichero%nproc;

        		/* Bucle de lectura del fichero */
			for (int i=0; i<tam; i++) {
				// ¿podría resolverse el error con este lseek
				//lseek(f, (p * nbloques_proceso + i) * 4096, SEEK_SET);
				if ((n = read(f, buf, sizeof(buf)))>0) {
					for (int j=0; j<n; j++) 
						if (buf[j]==caracter)
							contador++;
				}
				else break;
			}
			close(f);
                        write(tub[1], &contador, sizeof(contador));
                        close(tub[1]);
                        return 0;
                }

	}
	close(f);
        close(tub[1]);
        for (int p=0; p<nproc; p++) {
                read(tub[0], &n, sizeof(n));
                contador+=n;
                wait(NULL);
        }
        close(tub[0]);

	printf("%d\n", contador);

	return 0;
}
