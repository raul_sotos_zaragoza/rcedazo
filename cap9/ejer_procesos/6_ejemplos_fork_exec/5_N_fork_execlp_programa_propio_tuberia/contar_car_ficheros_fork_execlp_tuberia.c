// Ejemplo que cuenta las líneas de los ficheros recibidos como argumentos
// usando un proceso que ejecuta el mandato "wc -l" (fork+exec) para cada uno.

#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>

int main(int argc, char *argv[]) {
	int rf, i, nprocs = 0, ncar, ntot = 0;
	int tub[2];
	if (argc<3) {
		fprintf(stderr, "Uso: %s caracter fichero...\n", argv[0]);
		return 1;
	}
	pipe(tub);
	for (i=2; i<argc; i++) {
		if ((rf = fork()) == 0) {
			close(tub[0]);
			dup2(tub[1],1);
			close(tub[1]);
			execlp("./contar_car_fichero", "contar_car_fichero", argv[1], argv[i], NULL);
			perror("exec");
			return 1;
		}
		else if (rf == -1)
			perror("fork");
		else // fork OK; padre	
			nprocs ++;
	}
	close(tub[1]);
	FILE * ftub = fdopen(tub[0], "r");
	for (i=0; i<nprocs; i++) {
		fscanf(ftub, "%d%*s\n", &ncar);
		ntot+=ncar;
		wait(NULL);
	}
	fclose(ftub);
	printf("total %d\n", ntot);
		
	return 0;
}
