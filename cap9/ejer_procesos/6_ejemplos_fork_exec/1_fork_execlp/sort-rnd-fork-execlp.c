// Imprime una lista ordenada de números aleatorios usando fork y execl
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <sys/times.h>
#include <sys/wait.h>

int main(int argc, char *argv[]){
        unsigned int niter;

	if (argc!=2)  {
                fprintf (stderr, "Uso: %s nºvalores\n", argv[0]);
                return(1);
        }
	niter=atoi(argv[1]);

	// calculo una semilla aleatoria
	srand(getpid()*times(NULL));

	// creo una tubería
	int tub[2];
	pipe(tub);


	if (fork()==0) {
		close(tub[1]);
		dup2(tub[0], 0);
		close(tub[0]);
		execlp("sort", "sort", "-n", NULL); 
		perror("error en exec");
		return 1;
	}
	else {
		close(tub[0]);
		for (int i=0; i<niter; i++)
			dprintf(tub[1], "%d\n", rand());
		close(tub[1]);
		wait(NULL);
	}
	return 0;
}

