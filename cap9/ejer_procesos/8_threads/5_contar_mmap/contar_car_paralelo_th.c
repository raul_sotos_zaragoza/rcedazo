/* Cuenta cu�ntas veces aparece un car�cter en un fichero usando read/write.
   Versi�n paralela con threads */

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

struct info {
        unsigned char car;
	char *ini;
        int ncar;
};

void *cuenta(void *arg){
        struct info *inf = arg;
        int contador=0;
	char *v = inf->ini;
	for (int i=0; i<inf->ncar; i++)
		if (v[i]==inf->car) contador++;
        free(inf);
        return (void *)(long)contador;
}


int main(int  argc, char *argv[]) {
	int f, nthr, contador=0, ncar_thr;
	char caracter;
	char *v;
	struct stat bstat;
        void *res;

        if (argc!=4) {
                fprintf (stderr, "Uso: %s caracter fichero n�threads\n", argv[0]);
                return 1;
        }

	/* Por simplificar, se supone que el car�cter a contar corresponde
	   con el primero del primer argumento */
	caracter=argv[1][0];

        nthr=atoi(argv[3]);
        pthread_t thid[nthr];


	/* Abre el fichero para lectura */
	if ((f=open(argv[2], O_RDONLY))<0) {
		perror("No puede abrirse el fichero");
		return 1;
	}

	/* Averigua la longitud del fichero */
	if (fstat(f, &bstat)) {
		perror("Error en fstat del fichero");
		close(f);
		return 1;
	}

	/* Se proyecta todo el fichero */
	if ((v=mmap((caddr_t) 0, bstat.st_size, PROT_READ, MAP_SHARED, f, 0))
				==MAP_FAILED) {
		perror("Error en la proyeccion del fichero");
		close(f);
		return 1;
	}

        // �cu�ntos caracteres le toca a cada thread?
        ncar_thr = bstat.st_size/nthr;


	/* Se cierra el fichero */
	close(f);

        // bucle de creaci�n de threads
        for (int i=0; i<nthr; i++) {
                struct info *inf = malloc(sizeof(struct info));
                inf->car = caracter;
                inf->ini = v + i*ncar_thr;
                inf->ncar = ncar_thr;
		// el �ltimo se lleva el resto
                if (i==nthr-1) inf->ncar+=bstat.st_size%nthr;
                if (pthread_create(&thid[i], NULL, cuenta, inf)!=0) {
                        perror("pthread_create");
                        return 1;
                }
	}

        for (int i=0; i<nthr; i++) {
               if (pthread_join(thid[i], &res)!=0){
                        perror("pthread_join");
                        return 1;
                }
                contador+=(long)res;
        }
	/* Se eliminan las proyecciones */
	munmap(v, bstat.st_size);

	printf("%d\n", contador);
	return 0;
}

