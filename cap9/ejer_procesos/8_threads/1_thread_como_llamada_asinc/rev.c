// Invierte la cadena recibida como argumento usando un thread.
// Intenta remarcar que crear un thread es como una llamada asíncrona
// a una función.

#include <pthread.h>
#include <stdio.h>
#include <string.h>

void *strrev(void *arg) {
	char aux, *cadena = arg;
	int n=strlen(cadena);
	for (int i=0; i<n/2; i++) {
		aux=cadena[i];
		cadena[i]= cadena[n-1-i];
		cadena[n-1-i]=aux;
	}
	return cadena;
}

int main(int argc, char *argv[]) {
	pthread_t tid;
	void *res;
	if (argc!=2) {
		fprintf(stderr, "Uso: %s cadena\n", argv[0]);
		return 1;
	}
	// Equivale a llamar a una función:
	// res = strrev(argv[1]);
	// pero asíncrono: llamador no espera por la respuesta
	if (pthread_create(&tid, NULL, strrev, argv[1])!=0) {
		perror("pthread_create");
		return 1;
	}

	// ....... hace otras cosas ......
	
	// recoge el resultado
	if (pthread_join(tid, &res)!=0) {
		perror("pthread_join");
		return 1;
	}
	printf("%s\n", (char *)res);
	return 0;
}
