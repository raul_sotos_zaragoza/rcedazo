// Recibe por la cola de servicio una petición formada por el
// nombre de un fichero y el de una cola de respuesta, calcula cuántas
// líneas tiene el fichero y envía el resultado a la cola de respuesta.
// Versión paralela del servidor basada en threads.
#include <sys/types.h>
#include <sys/mman.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <pthread.h>
#include <sys/stat.h>

struct info {
        char *fich;
        char *cola;
};

void * cuenta_lineas(void *arg) {
        int fd, contador=0, res;
        char *p;
        struct stat bstat;
	struct info *inf = arg;

	printf("empieza servicio %s\n", inf->fich);

        /* Abre el fichero origen para lectura */
        if ((fd=open(inf->fich, O_RDONLY))<0) {
                perror("No puede abrirse el fichero pedido");
                return NULL;
        }
	free(inf->fich);
        /* Averigua la longitud del fichero pedido */
        if (fstat(fd, &bstat)) {
                perror("Error en fstat del fichero pedido");
                close(fd);
                return NULL;
        }
        if ((p=mmap((caddr_t) 0, bstat.st_size, PROT_READ, MAP_SHARED, fd, 0))
                                ==MAP_FAILED) {
                perror("Error en la proyeccion del fichero pedido");
                close(fd);
                return NULL;
        }
        /* Se cierra el fichero */
        close(fd);

        /* Bucle de acceso */
        for (int i=0; i<bstat.st_size; i++)
                if (p[i]=='\n') contador++;

        munmap(p, bstat.st_size);
	// obtiene acceso a la cola de respuesta
        if ((res=open(inf->cola, O_WRONLY))<0)
        	perror("No puede abrirse la cola de respuesta");
	free(inf->cola);
	if (res!=-1) {
		// envía la respuesta
		if (write(res, &contador, sizeof(contador))<0)
               		perror("No puede escribir en la cola de respuesta");
		close(res);
	}
	printf("fin servicio\n");
        return NULL;
}

int main(int  argc, char *argv[]) {
	char *fichero, *cola_respuesta;
	FILE *pet;
        pthread_t thid;
        pthread_attr_t tattr;

	/* crea la cola para recibir peticiones  */
        unlink("cola_servicio");
        if (mkfifo("cola_servicio", 0600)<0) {
                perror("No puede crearse la cola de peticiones");
                return 1;
	}
        pthread_attr_init(&tattr);
        pthread_attr_setdetachstate(&tattr,PTHREAD_CREATE_DETACHED);

	while (1) {
        	if ((pet=fopen("cola_servicio", "r"))==NULL) {
               		perror("No puede abrirse la cola de peticiones");
               		return 1;
        	}
		while (1) {
			// lee una petición
			int n=fscanf(pet, "%ms%ms", &fichero, &cola_respuesta);
			if (n==-1) {
				fclose(pet);
				break;
			}
			struct info *inf = malloc(sizeof(struct info));
			inf->fich = fichero;
			inf->cola = cola_respuesta;
			if (pthread_create(&thid, &tattr, cuenta_lineas, inf)!=0) {
				perror("pthread_create");
				return 1;
			}

		}
	}
        unlink("cola_servicio");
        pthread_attr_destroy(&tattr);
        return 0;
}

