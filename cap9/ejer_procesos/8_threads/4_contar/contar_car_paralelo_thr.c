/* Cuenta cuántas veces aparece un carácter en un fichero usando read/write.
   Versión paralela con threads */
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

struct info {
	unsigned char car;
	int f;
};

void *cuenta(void *arg){
	struct info *inf = arg;
	int n, contador=0;
        unsigned char buf[4096];
	/* Bucle de lectura del fichero */
	while ((n = read(inf->f, buf, sizeof(buf)))>0) {
		for (int i=0; i<n; i++)
			if (buf[i]==inf->car) contador++;
	}
	free(inf);
	return (void *)(long)contador;
}
int main(int  argc, char *argv[]) {
	int f, nthr, contador=0;
	unsigned char caracter;
	void *res;

        if (argc!=4) {
                fprintf (stderr, "Uso: %s caracter fichero nºthreads\n", argv[0]);
                return 1;
        }

        /* Por simplificar, se supone que el carácter a contar corresponde
           con el primero del primer argumento */
        caracter=argv[1][0];

        nthr=atoi(argv[3]);
        pthread_t thid[nthr];

        /* Abre el fichero */
        if ((f=open(argv[2], O_RDONLY))<0) {
		perror("No puede abrirse el archivo");
		return(1);
	}

	// bucle de creación de threads
        for (int i=0; i<nthr; i++) {
		struct info *inf = malloc(sizeof(struct info));
		inf->car = caracter;
		inf->f = f;
        	if (pthread_create(&thid[i], NULL, cuenta, inf)!=0) {
                	perror("pthread_create");
                	return 1;
        	}

	}
        for (int i=0; i<nthr; i++) {
               if (pthread_join(thid[i], &res)!=0){
                        perror("pthread_join");
                        return 1;
                }
                contador+=(long)res;
        }
	close(f);

	printf("%d\n", contador);

	return 0;
}
