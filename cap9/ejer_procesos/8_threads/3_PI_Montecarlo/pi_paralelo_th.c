// Versión paralela del cálculo de PI basada en threads.
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/times.h>
#include <math.h>

void * calculo(void *arg) {
        unsigned int nhits=0, niter_thr = (long)arg;
	double x, y;

	for (int i=0; i<niter_thr; i++) {
		// genero un punto formado por dos aleatorios en el rango [0,2]
		x = 2.0 * rand()/RAND_MAX;
		y = 2.0 * rand()/RAND_MAX;
		// compruebo si el punto está dentro de un círculo de radio 1
		// con centro en (1,1)
		if (((x-1)*(x-1) + (y-1)*(y-1)) <=1)
               		nhits++;
	}
	return (void *)(long)nhits;
}
int main(int argc, char *argv[]){
        unsigned int nhits=0, niter, nthr, niter_thr;
	void *res;
	double pi;

	if (argc!=3)  {
                fprintf (stderr, "Uso: %s nºiteraciones nºthreads\n", argv[0]);
                return 1;
        }
	niter=atoi(argv[1]);
	nthr=atoi(argv[2]);
	// cuántas iteraciones hará cada thread (redondeando por exceso)
	niter_thr = (niter + nthr -1)/nthr;

	// calculo una semilla aleatoria
	srand(getpid()*times(NULL));

	pthread_t thid[nthr];

	for (int i=0; i<nthr; i++) 
		if (pthread_create(&thid[i], NULL, calculo,
					(void *)(long)niter_thr)!=0){
			perror("pthread_create");
			return 1;
		}

	for (int i=0; i<nthr; i++) {
		if (pthread_join(thid[i], &res)!=0){
			perror("pthread_join");
			return 1;
		}
		nhits+=(long)res;
	}
	pi = 4.0 * nhits/niter;
	printf("PI estimado %lf PI real %lf\n", pi, M_PI);
	return 0;
}

