// En este programa dos threads incrementan concurrentemente una variable
// global. Al no existir sincronización, el resultado puede ser incorrecto
// ya que se produce una condición de carrera que se estudiará en el
// tema de comunicación y sincronización.
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

long c=0;
void *f(void *arg) {
	long niter = (long)arg;
	for (long i=0; i<niter; i++)
		c++;
	return 0;
}

int main(int argc, char *argv[]) {
	if (argc!=2) {
		fprintf(stderr, "Uso: %s nºiteraciones\n", argv[0]);
		return 1;
	}
	long niter = atol(argv[1]);
	pthread_t tid1, tid2;
	if (pthread_create(&tid1, NULL, f, (void *)niter)!=0){
		perror("pthread_create");
		return 1;
	}
	if (pthread_create(&tid2, NULL, f, (void *)niter)!=0){
		perror("pthread_create");
		return 1;
	}
	if (pthread_join(tid1, NULL)!=0){
		perror("pthread_join");
		return 1;
	}
	if (pthread_join(tid2, NULL)!=0){
		perror("pthread_join");
		return 1;
	}
	printf("valor esperado %ld valor obtenido %ld\n", 2*niter, c);
	return 0;
}
