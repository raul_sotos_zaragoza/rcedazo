// Imprime una lista ordenada de números aleatorios usando execlp
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include <sys/times.h>

int main(int argc, char *argv[]){
        unsigned int niter;
	char fichero[]="temp-XXXXXX";
	system("rm -f temp-*");

	if (argc!=2)  {
                fprintf (stderr, "Uso: %s nºvalores\n", argv[0]);
                return(1);
        }
	niter=atoi(argv[1]);

	// calculo una semilla aleatoria
	srand(getpid()*times(NULL));

	// creo un fichero temporal
	int f = mkstemp(fichero);

	for (int i=0; i<niter; i++)
		dprintf(f, "%d\n", rand());

	execlp("sort", "sort", "-n", fichero, NULL); 

	// si no falla exec, no debería ejecutarse esta parte
	perror("error en exec");
	return 0;
}

