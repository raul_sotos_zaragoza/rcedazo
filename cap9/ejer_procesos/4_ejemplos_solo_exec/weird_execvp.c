// programa que realiza un exec de sí mismo
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char *argv[]){
	printf("arranca el programa %s: PID %d\n", argv[0], getpid());

	// imprime todos los argumentos
	for (int i=0; i<argc; i++)
		printf("argv[%d] = %s\n", i, argv[i]);

	if (argc==1)
		printf("el programa no hace más exec\n");
	else {
		printf("el programa hace exec\n");
		argv[argc-1]=NULL;

		execvp(argv[0], argv); 
		// si no falla exec, no debería ejecutarse esta parte
		perror("error en exec");
	}
	return 0;
}
