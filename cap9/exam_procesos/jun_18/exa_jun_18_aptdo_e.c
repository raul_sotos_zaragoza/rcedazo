#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <signal.h>
#include <sys/wait.h>
#define TAM_BUFF 10

// NUEVO
pid_t pid1;
// Función de tratamiento de la alarma
void tratar_alarma(int signal) {
	//Mata al proceso pid1, que es el proceso hijo
	printf("Mata al proceso hijo\n");
	kill(pid1, SIGKILL);
}

int main(int argc, char* argv[]) {
	pid_t pid2, tub1[2], tub2[2];
	int fd=0;
	int leidos;
	char buffer[TAM_BUFF];
	pipe(tub1);
	pipe(tub2);
	pid1=fork();
	if (pid1==0){
		close(tub1[1]);
		pid2 = fork();
		if (pid2 == 0){
			close(tub1[0]);
			close(tub2[1]);
			leidos = read(tub2[0], buffer, TAM_BUFF);
			while (leidos >0){
				write(1, buffer, leidos);
				leidos = read(tub2[0], buffer, TAM_BUFF);
			}
			close(tub2[0]);
		}
		else {
			close(tub2[0]);
			leidos = read(tub1[0], buffer, TAM_BUFF);
			while (leidos >0){
				write (tub2[1], buffer, leidos);
				leidos = read(tub1[0], buffer, TAM_BUFF);
			}
			close(tub1[0]);
			close(tub2[1]);
		}
	}
	else {
		close(tub1[0]);
		close(tub2[0]);
		close(tub2[1]);
		fd = open("datos.txt", O_RDONLY);
		leidos = read(fd, buffer, TAM_BUFF);
		while (leidos > 0) {
			write(tub1[1], buffer, TAM_BUFF);
			leidos = read(fd, buffer, TAM_BUFF);
		}
		close(tub1[1]);
		close(fd);
		
		// NUEVO
		struct sigaction accion;
		accion.sa_handler = &tratar_alarma;
		sigaction(SIGALRM, &accion, NULL);
		alarm(5); // Establece el temporizador
		waitpid(pid1,NULL,0);
	}
	exit(0);
}
