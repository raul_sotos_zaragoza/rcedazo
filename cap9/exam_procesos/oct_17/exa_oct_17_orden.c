// forzando orden de ejecución de la solución
// e imprimiendo más información
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#define TAM_BUFF 10
pid_t pid1, pid2;
int main(int argc, char* argv[]) {
	int i=0;
	int fd=0;
	int fd2=0;
	char buffer[TAM_BUFF]="abcdefghij";
	fd = creat("fich1.txt", 0622);
	pid1=fork();
	pid2=fork();

	// forzando el orden planteado en la solución
	// primero padre (no sleep)
	// después segundo hijo
	if ((pid1!=0) && (pid2==0)) sleep(1);
	// después primer hijo
	if ((pid1==0) && (pid2!=0)) sleep(2);
	// después nieto
	if ((pid1==0) && (pid2==0)) sleep(3);

	if (pid1==0) {
		i=1;
		fd2 = dup(fd);
		write(fd2, buffer, TAM_BUFF);
	}
	else {
		if (pid2 == 0) {
			i=2;
			fd2 = open("fich1.txt", O_WRONLY);
			write(fd2, buffer, TAM_BUFF);
		}
		else {
			i=3;
			write(fd, buffer, TAM_BUFF);
			link("fich1.txt","fich2.txt");
			fd2 = open("fich2.txt", O_WRONLY);
			write(fd2,buffer, TAM_BUFF);
		}
	}
	printf("pid1:%d; pid2:%d; i:%d; uid:%d; gid:%d; euid:%d; egid:%d; fd:%d; fd2:%d (PID %d PPID %d)\n",
	   pid1, pid2, i, getuid(), getgid(), geteuid(), getegid(), fd, fd2,
	   getpid(), getppid());
	close(fd);
	close(fd2);

	// El padre espera para ver cómo ha quedado todo.
	// Comentar ese bloque para forzar huérfanos.
	/**/ if ((pid1!=0) && (pid2!=0)) {
		sleep(4);
		system("ps | grep exa_oct");
		system("ls -l fich1.txt fich2.txt");
		system("cat fich1.txt");
	} /**/
	return 0;
} 
