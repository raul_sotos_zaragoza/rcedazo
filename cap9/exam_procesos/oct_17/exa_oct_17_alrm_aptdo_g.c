// apartado g: alarma
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <signal.h>
#include <stdlib.h>

// NUEVO
void tratar_alarma(int signal) {
	printf("%d\n", signal); //Imprime por la salida estándar la señal correspondiente
	exit(0);
}
#define TAM_BUFF 10
pid_t pid1, pid2;
int main(int argc, char* argv[]) {
	int i=0;
	int fd=0;
	int fd2=0;
	char buffer[TAM_BUFF]="abcdefghij";
	fd = creat("fich1.txt", 0622);
	pid1=fork();
	pid2=fork();
	if (pid1==0) {
		i=1;
		fd2 = dup(fd);
		write(fd2, buffer, TAM_BUFF);
	}
	else {
		if (pid2 == 0) {
			i=2;
			fd2 = open("fich1.txt", O_WRONLY);
			write(fd2, buffer, TAM_BUFF);
		}
		else {
			i=3;
			write(fd, buffer, TAM_BUFF);
			link("fich1.txt","fich2.txt");
			fd2 = open("fich2.txt", O_WRONLY);
			write(fd2,buffer, TAM_BUFF);

			// NUEVO
			struct sigaction accion;
			accion.sa_handler = tratar_alarma;
			sigaction(SIGALRM, &accion, NULL);
			alarm(5); // Establece el temporizador
			pause(); // Se bloquea hasta la llegada de la señal
		}
	}
	printf("pid1:%d; pid2:%d; i:%d; uid:%d; gid:%d; euid:%d; egid:%d; fd:%d; fd2:%d\n",
	   pid1, pid2, i, getuid(), getgid(), geteuid(), getegid(), fd, fd2);
	close(fd);
	close(fd2);
	return 0;
} 
