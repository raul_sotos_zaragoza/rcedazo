// apartado f: exec
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#define TAM_BUFF 10
pid_t pid1, pid2;
int main(int argc, char* argv[]) {
	int i=0;
	int fd=0;
	int fd2=0;
	char buffer[TAM_BUFF]="abcdefghij";
	fd = creat("fich1.txt", 0622);
	pid1=fork();
	pid2=fork();
	if (pid1==0) {
		i=1;
		fd2 = dup(fd);
		write(fd2, buffer, TAM_BUFF);
	}
	else {
		if (pid2 == 0) {
			i=2;
			fd2 = open("fich1.txt", O_WRONLY);
			write(fd2, buffer, TAM_BUFF);
		}
		else {
			execvp(argv[1], &argv[1]);
			perror("exec");
			return 1;
		}
	}
	printf("pid1:%d; pid2:%d; i:%d; uid:%d; gid:%d; euid:%d; egid:%d; fd:%d; fd2:%d\n",
	   pid1, pid2, i, getuid(), getgid(), geteuid(), getegid(), fd, fd2);
	close(fd);
	close(fd2);
	return 0;
} 
