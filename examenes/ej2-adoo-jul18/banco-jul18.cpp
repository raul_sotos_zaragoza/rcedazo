#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <cstring>
using namespace std;

class BIC {
	string codigo;
public:
	BIC(string c) : codigo(c) {}
	bool esCodigoInternacional() {
		string caixa("CAIXAES");
		size_t encontrada = codigo.find(caixa);
		if (encontrada != std::string::npos) {
			cout << "Es internacional" << '\n';
			return true;
		}
		cout << "No es internacional" << '\n';
		return false;
	}
};

class Extraccion;

class IComision {
public:
	virtual float getTotal() = 0;
	static IComision *metodoFabricacionComision(Extraccion *);
};

class Extraccion {
	BIC banco;
	float cantidad;
	IComision * comision;
public:
	Extraccion(float c, BIC b) : cantidad(c), banco(b) {}
	float getTotal() {
		comision = IComision::metodoFabricacionComision(this);
		return comision->getTotal();
	}
	bool esCodigoInternacional() {
		return banco.esCodigoInternacional();
	}
	float getCantidad() {
		return cantidad;
	}
};

class ComisionInternacional : public IComision {
	friend class IComision;
	Extraccion *extraccion;
	ComisionInternacional(Extraccion *e) : extraccion(e) {}
public:
	float getTotal() {
		return extraccion->getCantidad() * 1.40;
	}
};

class ComisionFija : public IComision {
	friend class IComision;
	Extraccion *extraccion;
	ComisionFija(Extraccion *e) : extraccion(e) {}
public:
	float getTotal() {
		return extraccion->getCantidad() + 5;
	}
};

IComision * IComision::metodoFabricacionComision(Extraccion * e) {
	if (e->esCodigoInternacional())
		return new ComisionInternacional(e);
	else
		return new ComisionFija(e);
}

int main() {
	float cantidad;
	cout << "\Cantidad a extraer: ";
	cin >> cantidad;
	BIC banco("CAIXBB888");
	Extraccion miExtraccion(cantidad, banco);
	cout << "\nTotal de la operacion: " << miExtraccion.getTotal() << " euros.";

	return 0;
}