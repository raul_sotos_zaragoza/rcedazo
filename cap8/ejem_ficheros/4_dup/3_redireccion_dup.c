#include <sys/types.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

int main(int argc, char *argv[]) {
	pid_t pid; 
	int status, fd; 

	if (argc!=2)	{
		fprintf (stderr, "Uso: %s archivo\n", argv[0]);
		return(1);
	}

	fd = creat(argv[1], 0666); 
	if (fd < 0) {
		perror("creat");
		exit(1);
	} 
	pid = fork(); 
	switch(pid) { 
		case -1:	 // error
			perror("fork"); exit(1); 
		case 0: 		 //proceso hijo ejecuta "ls"	
			close(1); 
			dup(fd); //el descriptor se copia en el 1 
			close(fd); 
			execlp("ls","ls",NULL); 
			perror("execlp"); exit(1); 
		default: 	//proceso padre 
			close(fd); 
			while (pid != wait(&status)); 
	 } 
	 return 0; 
} 
