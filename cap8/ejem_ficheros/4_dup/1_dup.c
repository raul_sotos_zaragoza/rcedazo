#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

static void muestra_fichero(char *f){
	char mandato[256];
	printf("------- Contenido de %s ------------\n", f);
	snprintf(mandato, sizeof(mandato), "cat %s", f);
	system(mandato);
	printf("\n-------------------------------------\n");
}

int main(int argc, char **argv) { 
	int fd1, fd2, fd3; 

	if (argc!=2) {
                fprintf(stderr, "Uso: %s fichero_destino\n", argv[0]);
                return 1;
	}
	//Crea fichero
	fd1 = creat(argv[1], 0666); 
	if (fd1 < 0) { //error apertura 
	   perror("creat"); 
	   exit(1); 
	} 
	fd2=dup(fd1);
	//Abre el mismo fichero
	fd3 = open(argv[1], O_RDWR); 
	if (fd3 < 0) { //error apertura 
	   perror("open"); 
	   exit(1); 
	} 
	printf("fd1(creación)=%d fd2(dup)=%d fd3(2º open)=%d\n",fd1, fd2, fd3);

	char *cadena = "1111";
	write(fd1, cadena, strlen(cadena));
	printf("Ha escrito %s con fd1 (pulsa return para seguir)\n", cadena);
	muestra_fichero(argv[1]);
	getchar();

	cadena = "2222";
	write(fd2, cadena, strlen(cadena));
	printf("Ha escrito %s con fd2 (pulsa return para seguir)\n", cadena);
	muestra_fichero(argv[1]);
	getchar();

	cadena = "3333";
	write(fd3, cadena, strlen(cadena));
	printf("Ha escrito %s con fd3 (pulsa return para seguir)\n", cadena);
	muestra_fichero(argv[1]);
	getchar();

	close(fd1);
	close(fd2);
	close(fd3);
	return 0;
}
