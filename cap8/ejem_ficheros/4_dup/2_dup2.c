#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>


int main(int argc, char **argv) { 
	int fd; 

	if (argc!=2) {
                fprintf(stderr, "Uso: %s fichero_destino\n", argv[0]);
                return 1;
	}
	//Crea fichero
	fd = creat(argv[1], 0666); 
	if (fd < 0) { //error apertura 
	   perror("creat"); 
	   exit(1); 
	} 
	if(dup2(fd,1)<0) { // error dup2
	   perror("dup2"); 
	   exit(1); 
	} 
	printf("Hola mundo\n");
	return 0;
}
