#include <dirent.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>

int main(int argc, char **argv){/* directorio se pasa como argumento */
	DIR *dirp;
	struct dirent *dp;

	if (argc!=2) {
                fprintf(stderr, "Uso: %s directorio\n", argv[0]);
                return 1;
        }
	/* abre el directorio pasado como argumento */
	dirp = opendir(argv[1]);
	if (dirp == NULL)  {
		fprintf(stderr,"No puedo abrir %s\n", argv[1]);
	} else {
		struct stat st;

		/* lee entrada a entrada */
		while ((dp = readdir(dirp)) != NULL) {
			fstatat(dirfd(dirp), dp->d_name, &st,
					AT_SYMLINK_NOFOLLOW);
			printf("%s tamaño real %ld y asignado %ld (bytes)\n",
				dp->d_name, st.st_size, st.st_blocks*512);
		}
		closedir(dirp);  
	}
	return 0;
}
