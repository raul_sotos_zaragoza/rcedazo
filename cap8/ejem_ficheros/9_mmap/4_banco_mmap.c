#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <fcntl.h>

#define FICH "cuentas.dat"

struct cuenta {
        char titular[32];
        float saldo;
};

static int tam_fichero(void) {
	struct stat s;
	if (stat(FICH, &s)<0) {
		perror("error accediendo a fichero de cuentas");
       		return -1;
	}
	return s.st_size;
}
int imprimir_cuenta(int n_cnt) {
	int fd;
	struct cuenta *c;

	if (((n_cnt+1) * sizeof(struct cuenta)) > tam_fichero())
		return -1;
	fd=open(FICH, O_RDONLY);
	c=mmap(0, tam_fichero(), PROT_READ, MAP_SHARED, fd, 0);
	close(fd);
	printf("Titular %s saldo %f\n", c[n_cnt].titular, c[n_cnt].saldo);
	munmap(c, tam_fichero());
	return 0;
}
int operacion_cuenta(int n_cnt, float val) {
	int fd;
	struct cuenta *c;

	if (((n_cnt+1) * sizeof(struct cuenta)) > tam_fichero())
		return -1;
	fd=open(FICH, O_RDWR);
	c=mmap(0, tam_fichero(), PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0);
	close(fd);
	c[n_cnt].saldo+=val;
	printf("Saldo actual %f\n", c[n_cnt].saldo);
	munmap(c, tam_fichero());
	return 0;
}

int crear_cuenta(char tit[], float sal_ini) {
	int fd, pos;
	struct cuenta c;

	if ((fd=open(FICH, O_WRONLY|O_CREAT|O_APPEND, 0600))<0)  {
		perror("error creando fichero de cuentas");
       		return -1;
	}
	strcpy(c.titular, tit);
	c.saldo = sal_ini;
	write(fd, &c, sizeof(struct cuenta));
	pos = lseek(fd, 0, SEEK_CUR);
	return (pos / sizeof(struct cuenta))-1;
}

int main(int argc, char *argv[]) {
	if ((argv[1]!=NULL) && strcmp(argv[1],"C")==0 && (argv[2]!=NULL) &&
                (argv[3]!=NULL))
                printf("Cuenta creada %d\n", crear_cuenta(argv[2], atof(argv[3])));
        else if ((argv[1]!=NULL) && strcmp(argv[1],"I")==0 && (argv[2]!=NULL))
                imprimir_cuenta(atoi(argv[2]));
        else if ((argv[1]!=NULL) && strcmp(argv[1],"O")==0 && (argv[2]!=NULL) &&
                (argv[3]!=NULL))
                operacion_cuenta(atoi(argv[2]), atof(argv[3]));
        else
                fprintf(stderr, "Usos del programa:\n\
    ./Banco C nombre_titular saldo_inicial # crear cuenta\n\
    ./Banco I num_cuenta # imprimir datos de cuenta\n\
    ./Banco O num_cuenta cantidad # operación sobre cuenta\n");
	return 0;
}

