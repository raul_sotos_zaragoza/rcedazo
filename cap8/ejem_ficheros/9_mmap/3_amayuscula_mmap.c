
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <ctype.h>

int main(int  argc, char **argv) {
	int i, fd;
	char *p;
	struct stat bstat;

	if (argc!=2)  {
		fprintf (stderr, "Uso: %s archivo\n", argv[0]);
		return(1);
	}

	/* Abre el archivo */
	if ((fd=open(argv[1], O_RDWR))<0) {
		perror("No puede abrirse el archivo");
		return(1);
	}

	/* Averigua la longitud del archivo */
	if (fstat(fd, &bstat)<0) {
		perror("Error en fstat del archivo");
		close(fd);
		return(1);
	}

	/* Se proyecta el archivo */
	if ((p=mmap((caddr_t) 0, bstat.st_size, PROT_READ|PROT_WRITE,
			MAP_SHARED, fd, 0)) == (void *)MAP_FAILED) {
		perror("Error en la proyeccion del archivo");
		close(fd);
		return(1);
	}

	/* Se cierra el archivo */
	close(fd);

	/* Bucle de acceso que sustituye las min�sculas por may�sculas */
	for (i=0; i<bstat.st_size; i++)
		if (islower(p[i]))
			p[i]=toupper(p[i]);

	/* Se elimina la proyecci�n */
	munmap(p, bstat.st_size);

	return(0);
}
