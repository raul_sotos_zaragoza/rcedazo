#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

int main(int  argc, char **argv)
{
        int fd_ent, fd_sal;
	long tam;
	char *p_org, *p_dst;
	struct stat bstat;

        if (argc!=3) {
                fprintf(stderr, "Uso: %s fichero_origen fichero_destino\n", argv[0]);
                return 1;
        }
        //Abre fichero origen o entrada 
        fd_ent = open(argv[1], O_RDONLY);
        if (fd_ent < 0) {
           perror("Error apertura fichero origen");
           exit(1);
        }
        //Crea fichero de destino o salida 
        fd_sal = open(argv[2], O_CREAT|O_TRUNC|O_RDWR, 0666);
        if (fd_sal < 0) {
           perror("Error creación fichero destino");
           close(fd_ent);
           exit(1);
        }
	/* Averigua la longitud del fichero origen */
	if (fstat(fd_ent, &bstat))
	{
		perror("Error en fstat del fichero origen");
		close(fd_ent); close(fd_sal);
		return(1);
	}
	tam=bstat.st_size;
	/* Establece la longitud del fichero destino */
	if (ftruncate(fd_sal, tam))
	{
		perror("Error en ftruncate del fichero destino");
		close(fd_ent); close(fd_sal);
		return(1);
	}
	/* Se proyecta el fichero origen */
	if ((p_org=mmap(0, tam, PROT_READ, MAP_SHARED, fd_ent, 0))==MAP_FAILED){
		perror("Error en la proyeccion del fichero origen");
		close(fd_ent); close(fd_sal);
		return(1);
	}
	/* Se proyecta el fichero destino */
	if ((p_dst=mmap(0, tam, PROT_WRITE, MAP_SHARED, fd_sal, 0))==MAP_FAILED){
		perror("Error en la proyeccion del fichero destino");
		close(fd_ent); close(fd_sal);
		return(1);
	}

	/* Se cierran los ficheros */
	close(fd_ent); close(fd_sal);

	/* copia */
	memcpy(p_dst, p_org, tam);

	/* Se eliminan las proyecciones */
	munmap(p_org, tam);
	munmap(p_dst, tam);

	return(0);
}

