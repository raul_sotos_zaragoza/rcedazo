#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include "comun.h"


static void iniciaPosicion(posicion *p){
	p->x=NAN;
}
static double distancia(posicion a, posicion b){
	double d;
	d=sqrt(pow(a.x-b.x,2) + pow(a.y-b.y,2) + pow(a.z-b.z,2));
	return d;
}

int main(int argc, char **argv) {
	int fd, i, n;
	posicion *objetos;

        if (argc!=5)  {
                fprintf (stderr, "Uso: %s nºObjetosMóviles miposicionX miposicionY miposicionZ\n", argv[0]);
                return(1);
        }
	n=atoi(argv[1]);
	posicion miPosicion={.x=atof(argv[2]),.y=atof(argv[3]),.z=atof(argv[4])};

        //Crea fichero
        fd = open("OBJETOS", O_CREAT|O_TRUNC|O_RDWR, 0666);
        if (fd < 0) {
           perror("Error creación fichero");
           exit(1);
        }
        /* Establece la longitud del fichero */
        if (ftruncate(fd, n*sizeof(posicion)))
        {
                perror("Error en ftruncate del fichero");
                close(fd);
                return(1);
        }
	/* Se proyecta el fichero */
        if ((objetos=mmap(0, n*sizeof(posicion), PROT_READ|PROT_WRITE, MAP_SHARED,
			fd, 0))==MAP_FAILED){
                perror("Error en la proyeccion del fichero");
                close(fd);
                return(1);
        }
        close(fd);
	for (i=0; i<n; i++) iniciaPosicion(&objetos[i]);

	while(1) {
		int proximo=-1;
		double dist;
		double distanciaMin = INFINITY;
		for (i=0; i<n; i++) {
			if ((!isnan(objetos[i].x)) &&
				((dist=distancia(miPosicion, objetos[i]))<
					distanciaMin)) {
				distanciaMin=dist;
				proximo=i;
			}
		}
		if (proximo!=-1)
			printf("Objeto más cercano %d; distancia %lf\n",
				proximo, distanciaMin);
		else
			printf("No hay objetos cercanos\n");
		sleep(5);
	}
	close(fd);
	unlink("OBJETOS");
	return(0);
}
