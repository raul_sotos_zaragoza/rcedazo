#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include "comun.h"

static int leePosicion(posicion *p){
        printf("Introduzca posición del objeto (x,y,z): ");
        return scanf("%lf%lf%lf", &p->x, &p->y, &p->z)==3;
}

int main(int argc, char **argv) {
	int fd, n, id;
	posicion *objetos;
	if (argc!=3)  {
                fprintf (stderr, "Uso: %s idObjeto nObjetos\n", argv[0]);
                return(1);
        }
	n=atoi(argv[2]);
        id=atoi(argv[1]);
	if (id>=n) {
                fprintf (stderr, "Uso: %s idObjeto nObjetos\n", argv[0]);
                return(1);
        }

        // Abre fichero
        fd = open("OBJETOS", O_RDWR, 0666);
        if (fd < 0) {
           perror("Error apertura fichero");
           exit(1);
        }
        /* Establece la longitud del fichero */
        if (ftruncate(fd, n*sizeof(posicion)))
        {
                perror("Error en ftruncate del fichero");
                close(fd);
                return(1);
        }
        /* Se proyecta el fichero */
        if ((objetos=mmap(0, n*sizeof(posicion), PROT_WRITE, MAP_SHARED,
                        fd, 0))==MAP_FAILED){
                perror("Error en la proyeccion del fichero");
                close(fd);
                return(1);
	}
        close(fd);
	while (leePosicion(&objetos[id]));
	objetos[id].x=NAN;
	munmap(objetos, n*sizeof(posicion));
	return(0);
}
