#include <stdio.h>
#include <stdlib.h>
#include <string.h>
int main(int argc, char *argv[]) {
	FILE *fich_ent, *fich_sal;
	if (argc!=3) {
		fprintf(stderr, "Uso: %s fichero palabra\n", argv[0]);
        	return 1;
	}
	if ((fich_ent=fopen(argv[1], "r")) == NULL) { 
		fprintf(stderr, "error abriendo el fichero\n");
        	return 1;
	}
	if ((fich_sal=fopen(argv[2], "w")) == NULL) { 
		fprintf(stderr, "error creando el fichero\n");
        	return 1;
	}
	int byte;
	while ((byte=fgetc(fich_ent))!=EOF)
		fputc(byte, fich_sal);
	fclose(fich_ent);
	fclose(fich_sal);
	return 0;
}
