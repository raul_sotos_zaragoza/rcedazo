#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/stat.h>
#include <sys/sendfile.h>

int main(int argc, char **argv) { 
	int fd_ent, fd_sal; 

        if (argc!=3) {
                fprintf(stderr, "Uso: %s fichero_origen fichero_destino\n", argv[0]);
                return 1; 
        }
	//Abre fichero origen o entrada 
	fd_ent = open(argv[1], O_RDONLY); 
	if (fd_ent < 0) {  //error apertura
	   perror("open"); //transforma errno en un mensaje de error
	   exit(1); 
	} 
	// informa de que se trata de un acceso secuencial
	posix_fadvise(fd_ent, 0, 0, POSIX_FADV_SEQUENTIAL);
	//Crea fichero de destino o salida 
	fd_sal = creat(argv[2], 0666); 
	if (fd_sal < 0) { //error apertura 
	   close(fd_ent); 
	   perror("creat"); 
	   exit(1); 
	} 
	//averigua el tamaño del fichero
	int err;
	struct stat st;
	err = fstat(fd_ent, &st); 
	if (err < 0) {  //error en stat
	   perror("stat"); //transforma errno en un mensaje de error
	   exit(1); 
	} 
	if (sendfile(fd_sal, fd_ent, NULL, st.st_size)!=st.st_size) {
	      perror("sendfile");
	      close(fd_ent);
	      close(fd_sal);
	      exit(1);
	}
	close(fd_ent);
	close(fd_sal);
	return 0;
}
