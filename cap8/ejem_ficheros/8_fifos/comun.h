
typedef struct posicion {
	double x;
	double y;
	double z;
} posicion;

typedef struct mensaje {
	unsigned int id;
	posicion posicionActual;
} mensaje;
