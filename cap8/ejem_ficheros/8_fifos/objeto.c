#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include "comun.h"

static int leePosicion(posicion *p){
	printf("Introduzca posición del objeto (x,y,z): ");
	return scanf("%lf%lf%lf", &p->x, &p->y, &p->z)==3;
}

int main(int argc, char **argv) {
	int fd, id;

        if (argc!=2)  {
                fprintf (stderr, "Uso: %s idObjeto\n", argv[0]);
                return(1);
        }
	id=atoi(argv[1]);

	/* Abre el FIFO */
	if ((fd=open("FIFO", O_WRONLY))<0) {
		perror("No puede abrirse el FIFO");
		return(1);
	}
	mensaje m = {.id=id};
	while (leePosicion(&m.posicionActual))
		write(fd, &m, sizeof(m));
	close(fd);

	return(0);
}
