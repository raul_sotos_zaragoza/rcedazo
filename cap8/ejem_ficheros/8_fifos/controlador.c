#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>
#include "comun.h"

typedef struct infoObjetoMovil {
	double recorrido;
	posicion ultimaPosicion;
} infoObjetoMovil;

static void iniciaObjeto(infoObjetoMovil *o){
	o->recorrido=0; o->ultimaPosicion.x=NAN;
}
static void imprimeObjeto(int id, const infoObjetoMovil *o){
	printf("Objeto %d\n\trecorrido %lf\n\tultima posición=(%lf,%lf,%lf)\n",
		id, o->recorrido, o->ultimaPosicion.x, o->ultimaPosicion.y,
		o->ultimaPosicion.z);
}
static double distancia(posicion a, posicion b){
	double d;
	d=sqrt(pow(a.x-b.x,2) + pow(a.y-b.y,2) + pow(a.z-b.z,2));
	return d;
}

int main(int argc, char **argv) {
	int fd;

        if (argc!=2)  {
                fprintf (stderr, "Uso: %s nºObjetosMóviles\n", argv[0]);
                return(1);
        }
	unsigned int n=atoi(argv[1]);
	infoObjetoMovil objetos[n];
	for (int i=0; i<n; i++) iniciaObjeto(&objetos[i]);

	/* crea el FIFO */
	if (mkfifo("FIFO", 0600)<0) {
		perror("No puede crearse el FIFO");
		return(1);
	}
	/* Abre el FIFO */
	if ((fd=open("FIFO", O_RDONLY))<0) {
		perror("No puede abrirse el FIFO");
		return(1);
	}
	mensaje m;
	while (read(fd, &m, sizeof(m))==sizeof(m)) {
		if (m.id<n) {
			if (!isnan(objetos[m.id].ultimaPosicion.x))
				objetos[m.id].recorrido+=distancia(m.posicionActual,
					objetos[m.id].ultimaPosicion);
			objetos[m.id].ultimaPosicion=m.posicionActual;
			imprimeObjeto(m.id, &objetos[m.id]);
		}
	}
	close(fd);
	unlink("FIFO");
	return(0);
}
