#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int main(int argc, char **argv) { 
	int fd; 

	if (argc!=2) {
                fprintf(stderr, "Uso: %s fichero_entrada\n", argv[0]);
                return 1;
	}
	//Abre fichero origen o entrada
        fd = open(argv[1], O_RDONLY);
        if (fd < 0) {  //error apertura
           perror("open"); //transforma errno en un mensaje de error
           exit(1);
        }

	// se lee un entero
	int total;
	read(fd, &total, sizeof(int));
	printf("Entero leído %d\n", total);

	// se lee una cadena de 4 caracteres
	char cadena[5];
	read(fd, cadena, 4);
	cadena[4]='\0';
	printf("Cadena leída %s\n", cadena);

	// se lee un vector con tres números en coma flotante
	float m[3];
	read(fd, m, sizeof(m));
	for (int i=0; i<sizeof(m)/sizeof(float); i++)
		printf("número real leído: m[%d]=%f\n", i, m[i]);

	typedef struct registro{
		char nombre[8];
   		int id;
   		float altura, peso;
	} registro;
	// se lee un registro
	registro persona;
	read(fd, &persona, sizeof(registro));
	printf("registro leído: nombre=%s, id=%d, altura=%f, peso=%f\n",
	       	persona.nombre, persona.id, persona.altura, persona.peso);

	// se lee un vector con 2 registros
	registro individuos[2];
	read(fd, individuos, sizeof(individuos));
	for (int i=0; i<sizeof(individuos)/sizeof(registro); i++)
		printf("registro %d: nombre=%s, id=%d, altura=%f, peso=%f\n",
	       		i, individuos[i].nombre, individuos[i].id,
			individuos[i].altura, individuos[i].peso);

	close(fd);
	return 0;
}
