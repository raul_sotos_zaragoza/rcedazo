#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int main(int argc, char **argv) { 
	int fd; 

	if (argc!=2) {
                fprintf(stderr, "Uso: %s fichero_destino\n", argv[0]);
                return 1;
	}
	//Crea fichero de destino o salida 
	fd = creat(argv[1], 0666); 
	if (fd < 0) { //error apertura 
	   perror("creat"); 
	   exit(1); 
	} 

	// se escribe un entero
	int total = 1246; /* total es 00 00 04 DE */
	write(fd, &total, sizeof(int));

	// se escribe una cadena de caracteres
	char cadena[] = "1246";
	write(fd, cadena, strlen(cadena));
	/*Escribe los 4 caracteres ASCII 1, 2, 4 y 6  (sin el carácter nulo terminador) */

	// se escribe un vector con tres números en coma flotante
	float m[3] = {1.5, 2.6, 3.7};
	write(fd, m, sizeof(m));

	typedef struct registro{
		char nombre[8];
   		int id;
   		float altura, peso;
	} registro;
	// se escribe un registro
	registro persona = {.nombre="Juan",.id=9,.altura=1.85,.peso=75.5};
	write(fd, &persona, sizeof(registro));

	// se escribe un vector con 2 registros
	registro individuos[2];
	strcpy(individuos[0].nombre, "Luis");
	individuos[0].id=7;
	individuos[0].altura=1.62;
	individuos[0].peso=57.5;

	strcpy(individuos[1].nombre, "Pepa");
	individuos[1].id=5;
	individuos[1].altura=1.72;
	individuos[1].peso=60.5;
	write(fd, individuos, sizeof(individuos));

	close(fd);
	return 0;
}
