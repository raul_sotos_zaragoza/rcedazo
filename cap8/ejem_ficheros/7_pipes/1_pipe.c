#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>

int main(int  argc, char **argv) {
	int fd, i, n;

	if (argc!=2)  {
		fprintf (stderr, "Uso: %s archivo\n", argv[0]);
		return(1);
	}

	/* Abre el archivo */
	if ((fd=open(argv[1], O_RDONLY))<0) {
		perror("No puede abrirse el archivo");
		return(1);
	}
	//averigua el tamaño óptimo para acceder al fichero
        int err;
        struct stat st;
        err = fstat(fd, &st);
        if (err < 0) {  //error en stat
           perror("stat"); //transforma errno en un mensaje de error
           return(1);
        }
	int pd[2];
	pipe(pd);
	if (fork()!=0) {
        	/* proceso padre: bucle de lectura del fichero de entrada */
		close(pd[0]);
        	char buf[st.st_blksize];

		while ((n = read(fd, buf, sizeof(buf)))>0) {
			for (i=0; i<n; i++)
				if (islower(buf[i])) buf[i]=toupper(buf[i]);
			write(pd[1], buf, n);
		}
		close(pd[1]);
		wait(NULL);
	}
	else {
		close(fd);
		close(pd[1]);
		dup2(pd[0], 0);
		close(pd[0]);
		execlp("sort", "sort", NULL);
		perror("exec");
		exit(1);
	}
	close(fd);
	return(0);
}
