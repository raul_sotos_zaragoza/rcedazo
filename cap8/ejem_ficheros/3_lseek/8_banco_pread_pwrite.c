#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>

#define FICH "cuentas.dat"

struct cuenta {
        char titular[32];
        float saldo;
};

static int tam_fichero(void) {
	struct stat s;
	if (stat(FICH, &s)<0) {
		perror("error accediendo a fichero de cuentas");
       		return -1;
	}
	return s.st_size;
}
int imprimir_cuenta(int n_cnt) {
	int fd;
	struct cuenta c;

	if (((n_cnt+1) * sizeof(struct cuenta)) > tam_fichero())
		return -1;
	fd=open(FICH, O_RDONLY);
	pread(fd, &c, sizeof(struct cuenta), n_cnt * sizeof(struct cuenta));
	printf("Titular %s saldo %f\n", c.titular, c.saldo);
	close(fd);
	return 0;
}
int operacion_cuenta(int n_cnt, float val) {
	int fd;
	struct cuenta c;

	if (((n_cnt+1) * sizeof(struct cuenta)) > tam_fichero())
		return -1;
	fd=open(FICH, O_RDWR);
	pread(fd, &c, sizeof(struct cuenta), n_cnt * sizeof(struct cuenta));
	c.saldo+=val;
	printf("Saldo actual %f\n", c.saldo);
	pwrite(fd, &c, sizeof(struct cuenta), n_cnt * sizeof(struct cuenta));
	write(fd, &c, sizeof(struct cuenta));
	close(fd);
	return 0;
}

int crear_cuenta(char tit[], float sal_ini) {
	int fd, pos;
	struct cuenta c;

	if ((fd=open(FICH, O_WRONLY|O_CREAT|O_APPEND, 0600))<0)  {
		perror("error creando fichero de cuentas");
       		return -1;
	}
	strcpy(c.titular, tit);
	c.saldo = sal_ini;
	write(fd, &c, sizeof(struct cuenta));
	pos = lseek(fd, 0, SEEK_CUR);
	return (pos / sizeof(struct cuenta))-1;
}

int main(int argc, char *argv[]) {
	if ((argv[1]!=NULL) && strcmp(argv[1],"C")==0 && (argv[2]!=NULL) &&
                (argv[3]!=NULL))
                printf("Cuenta creada %d\n", crear_cuenta(argv[2], atof(argv[3])));
        else if ((argv[1]!=NULL) && strcmp(argv[1],"I")==0 && (argv[2]!=NULL))
                imprimir_cuenta(atoi(argv[2]));
        else if ((argv[1]!=NULL) && strcmp(argv[1],"O")==0 && (argv[2]!=NULL) &&
                (argv[3]!=NULL))
                operacion_cuenta(atoi(argv[2]), atof(argv[3]));
        else
                fprintf(stderr, "Usos del programa:\n\
    ./Banco C nombre_titular saldo_inicial # crear cuenta\n\
    ./Banco I num_cuenta # imprimir datos de cuenta\n\
    ./Banco O num_cuenta cantidad # operación sobre cuenta\n");
	return 0;
}

