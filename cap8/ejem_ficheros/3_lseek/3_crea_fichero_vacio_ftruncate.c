#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>

int main(int  argc, char **argv) {
	int fd, exponente;

	if (argc!=3)  {
		fprintf (stderr, "Uso: %s archivo tamaño (2^argv2)\n", argv[0]);
		return(1);
	}

	/* Crea el archivo */
	if ((fd=creat(argv[1], 0600))<0) {
		perror("No puede crearse el archivo");
		return(1);
	}
	exponente = atoi(argv[2]);
	ftruncate(fd, 1<<exponente);

	close(fd);
	return(0);
}
