
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <ctype.h>

int main(int  argc, char **argv) {
	int fd, i, n, cambio;

	if (argc!=2)  {
		fprintf (stderr, "Uso: %s archivo\n", argv[0]);
		return(1);
	}

	/* Abre el archivo */
	if ((fd=open(argv[1], O_RDWR))<0) {
		perror("No puede abrirse el archivo");
		return(1);
	}
	//averigua el tamaño óptimo para acceder al fichero
        int err;
        struct stat st;
        err = fstat(fd, &st);
        if (err < 0) {  //error en stat
           perror("stat"); //transforma errno en un mensaje de error
           return(1);
        }
        /* bucle de lectura del fichero de entrada */
        char buf[st.st_blksize];

	while ((n = read(fd, buf, sizeof(buf)))>0) {
		cambio = 0;
		for (cambio=0, i=0; i<n; i++) {
			if (islower(buf[i])) {
				buf[i]=toupper(buf[i]);
				cambio = 1;
			}
		}
		if (cambio) {
			lseek(fd, -n, SEEK_CUR);
			write(fd, buf, n);
		}
	}

	close(fd);
	return(0);
}
