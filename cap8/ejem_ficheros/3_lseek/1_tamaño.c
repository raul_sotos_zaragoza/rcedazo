#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>

int main(int  argc, char **argv) {
	int fd;
	off_t t;
	struct stat st;

	if (argc!=2)  {
		fprintf (stderr, "Uso: %s archivo\n", argv[0]);
		return(1);
	}

	if ((fd=open(argv[1], O_RDONLY))<0) {
		perror("No puede abrirse el archivo");
		return(1);
	}
	if ((t=lseek(fd, 0, SEEK_END))==-1) {
		perror("Error en lseek");
		return(1);
	}

	printf("Tamaño usando lseek %ld\n", t);

	if (fstat(fd, &st)<0) {
                perror("Error en fstat");
                return(1);
        }

        printf("Tamaño usando stat: real %ld y asignado %ld\n",
                                st.st_size, st.st_blocks*512);

	close(fd);
	return(0);
}
