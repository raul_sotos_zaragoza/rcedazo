#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

void copiaypega(char *origen, off_t offorg, size_t size, char *destino, off_t offdst){
	int fdorg, fddst;
	fdorg=open(origen, O_RDONLY);
	fddst=open(destino, O_WRONLY|O_CREAT);
	int n;
	char buffer[size];
	lseek(fdorg, offorg, SEEK_SET);
	n=read(fdorg, buffer, size);
	lseek(fddst, offdst, SEEK_SET);
	write(fddst, buffer, n);
	close(fdorg);
	close(fddst);
}
int main(int argc, char *argv[]) {
        if (argc!=6) {
                fprintf(stderr, "Uso: %s fichero_origen posicion_origen tamaño fichero_destino posicion_destino\n", argv[0]);
                return 1; 
        }
	copiaypega(argv[1], atol(argv[2]), atol(argv[3]),argv[4],atol(argv[5]));
	return 0;
}

