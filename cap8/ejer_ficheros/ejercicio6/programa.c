#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>


int main(int argc, char **argv) { 
	int fd1, fd2, fd3; 

	if (argc!=2) {
                fprintf(stderr, "Uso: %s fichero\n", argv[0]);
                return 1;
	}
	fd1 = creat(argv[1], 0666); 
	if (fd1 < 0) {
	   perror("creat"); 
	   exit(1); 
	} 
	fd2=dup(fd1);
	//Abre el mismo fichero
	fd3 = open(argv[1], O_RDWR); 
	if (fd3 < 0) {
	   perror("open"); 
	   exit(1); 
	} 
	char *cadena = "1111";
	write(fd1, cadena, strlen(cadena));
	lseek(fd1, 2, SEEK_CUR);

	cadena = "2222";
	write(fd2, cadena, strlen(cadena));

	cadena = "3333";
	lseek(fd3, 2, SEEK_CUR);
	write(fd3, cadena, strlen(cadena));

	close(fd1);
	close(fd2);
	close(fd3);
	return 0;
}
