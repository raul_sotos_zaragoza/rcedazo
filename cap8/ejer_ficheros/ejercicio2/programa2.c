#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/stat.h>

int main(int argc, char **argv) { 
	int fd_ent, fd_sal, n_read; 

        if (argc!=3) {
                fprintf(stderr, "Uso: %s fichero_origen fichero_destino\n", argv[0]);
                return 1; 
        }
	fd_ent = open(argv[1], O_RDONLY); 
	fd_sal = creat(argv[2], 0666); 
	char buffer[4096]; 
	while ((n_read = read(fd_ent, buffer, sizeof(buffer))) > 0) {
	   write(fd_sal, buffer, n_read);
	   lseek(fd_ent, n_read, SEEK_CUR);
	}
	close(fd_ent);
	close(fd_sal);
	
	return 0;
}
