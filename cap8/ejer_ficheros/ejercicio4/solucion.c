#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>

int main(int argc, char **argv) {
	int fd;
	struct stat st;
	char car1, car2, aux;

	if (argc!=2)  {
		fprintf (stderr, "Uso: %s archivo\n", argv[0]);
		return(1);
	}

	fd=open(argv[1], O_RDWR);
	fstat(fd, &st);

	for (int i=0; i<st.st_size/2; i++) {
		lseek(fd, i, SEEK_SET);
		read(fd, &car1, 1);
		lseek(fd, -i-1, SEEK_END);
		read(fd, &car2, 1);
		aux = car1;
		car1 = car2;
		car2 = aux;
		lseek(fd, i, SEEK_SET);
		write(fd, &car1, 1);
		lseek(fd, -i-1, SEEK_END);
		write(fd, &car2, 1);
	}

	close(fd);
	return(0);
}
