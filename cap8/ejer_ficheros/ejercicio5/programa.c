#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>

int main(int argc, char **argv) {
	int fd;
	struct stat st;
	char aux;
	char *p;

	if (argc!=2)  {
		fprintf (stderr, "Uso: %s archivo\n", argv[0]);
		return(1);
	}

	fd=open(argv[1], XX);
	fstat(fd, &st);

	p=mmap(NULL, st.st_size, XX, MAP_SHARED, fd, 0);
	close(fd);

	for (int i=0; i<st.st_size/2; i++) {
		aux = p[XX];
		p[XX] = p[st.st_size-XX];
		p[st.st_size-XX] = aux;
	}
	munmap(p, st.st_size);

	return(0);
}
