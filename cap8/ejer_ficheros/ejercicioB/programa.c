#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/stat.h>

int main(int argc, char **argv) { 
	int fd; 
	struct stat st;

	fd = creat("F", 0777); 
	fstat(fd, &st);
	printf("permisos: %o\n", st.st_mode & ~S_IFMT);
	close(fd);
	unlink("F");
	
	return 0;
}
