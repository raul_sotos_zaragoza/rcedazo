#include <dirent.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/stat.h>

int main(int argc, char **argv){
	DIR *dirp;
	struct dirent *dp;

	if (argc!=2) {
                fprintf(stderr, "Uso: %s directorio\n", argv[0]);
                return 1;
        }
	dirp = opendir(argv[1]);
	if (dirp == NULL)  {
		fprintf(stderr,"No puedo abrir %s\n", argv[1]);
	} else {
		struct stat st;

		while ((dp = readdir(dirp)) != NULL) {
			if (stat(dp->d_name, &st)<0) {
				perror("error en stat");
				return 1;
			}
			printf("%s tamaño %ld enlaces %ld\n",
				dp->d_name, st.st_size, st.st_nlink);
		}
		closedir(dirp);  
	}
	return 0;
}
