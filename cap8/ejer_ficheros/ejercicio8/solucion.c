#include <dirent.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/stat.h>

int main(int argc, char **argv){
	DIR *dirp;
	struct dirent *dp;

	if (argc!=2) {
                fprintf(stderr, "Uso: %s directorio\n", argv[0]);
                return 1;
        }
	dirp = opendir(argv[1]);
	if (dirp == NULL)  {
		fprintf(stderr,"No puedo abrir %s\n", argv[1]);
	} else {
		struct stat st;
		char entrada[4096];

		while ((dp = readdir(dirp)) != NULL) {
			snprintf(entrada, sizeof(entrada), "%s/%s",
					argv[1], dp->d_name);
			stat(entrada, &st);
			printf("%s tamaño %ld enlaces %ld\n",
				dp->d_name, st.st_size, st.st_nlink);
		}
		closedir(dirp);  
	}
	return 0;
}
