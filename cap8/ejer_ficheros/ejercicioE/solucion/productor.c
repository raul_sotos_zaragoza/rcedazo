#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char **argv) {
	int fd;

	if ((fd=open("FIFO", O_WRONLY))<0) {
		perror("No puede abrirse el FIFO");
		return(1);
	}
	srand(getpid());
	unsigned int num;
	while (1) {
		num=rand();
		write(fd, &num, sizeof(unsigned int));
		sleep(2);
	}
	close(fd);

	return(0);
}
