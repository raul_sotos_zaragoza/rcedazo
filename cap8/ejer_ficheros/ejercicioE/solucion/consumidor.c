#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char **argv) {
	int fd;

	unlink("FIFO");
	if (mkfifo("FIFO", 0600)<0) {
		perror("No puede crearse el FIFO");
		return(1);
	}
	if ((fd=open("FIFO", O_RDONLY))<0) {
		perror("No puede abrirse el FIFO");
		return(1);
	}
	unsigned int valor, n_valores=0;
	double total = 0;
	while (read(fd, &valor, sizeof(unsigned int))>0) {
		total+=valor;
		n_valores++;
		printf("media %.2lf\n", total/n_valores);
	}
	close(fd);
	unlink("FIFO");
	return(0);
}
