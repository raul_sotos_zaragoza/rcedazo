#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char **argv) {
	int fd;

	// apertura del FIFO "FIFO"
	if ((fd=open(XX))<0) {
		perror("No puede abrirse el FIFO");
		return(1);
	}
	srand(getpid());
	unsigned int num;
	while (1) {
		num=rand();
		// escritura del número en el FIFO
		write(XX);
		sleep(2);
	}
	close(fd);

	return(0);
}
