#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char **argv) {
	int fd;

	unlink("FIFO");
	// creación del FIFO "FIFO"
	if (mkfifo(XX)<0) {
		perror("No puede crearse el FIFO");
		return(1);
	}
	// apertura del FIFO "FIFO"
	if ((fd=open(XX))<0) {
		perror("No puede abrirse el FIFO");
		return(1);
	}
	unsigned int valor, n_valores=0;
	double total = 0;
	// lectura del FIFO
	while (read(XX)>0) {
		total+=valor;
		n_valores++;
		printf("media %.2lf\n", total/n_valores);
	}
	close(fd);
	unlink("FIFO");
	return(0);
}
