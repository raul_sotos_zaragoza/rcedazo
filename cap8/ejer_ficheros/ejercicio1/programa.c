#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/stat.h>

int main(int argc, char **argv) { 
	int fd_ent, fd_sal; 

        if (argc!=3) {
                fprintf(stderr, "Uso: %s fichero_origen fichero_destino\n", argv[0]);
                return 1; 
        }
	fd_ent = open(argv[1], XX); 
	fd_sal = creat(argv[2], XX); 
	char buffer[4096]; 
	while (read(fd_ent, buffer, sizeof(buffer)) > 0)
	   write(fd_sal, buffer, sizeof(buffer));
	close(fd_ent);
	close(fd_sal);
	
	return 0;
}
