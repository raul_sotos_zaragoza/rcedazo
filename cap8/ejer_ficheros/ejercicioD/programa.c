#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>

int main(int  argc, char **argv) {
	int fd, i, n;

	if (argc!=2)  {
		fprintf (stderr, "Uso: %s archivo\n", argv[0]);
		return(1);
	}

	int pd[2];
	pipe(pd);
	if (fork()!=0) {
		if ((fd=creat(argv[1], 0666))<0) {
			perror("No puede abrirse el archivo");
			return(1);
		}
		close(XX);
        	char buf[4096];

		while ((n = read(XX, buf, sizeof(buf)))>0) {
			for (i=0; i<n; i++)
				if (islower(buf[i])) buf[i]=toupper(buf[i]);
			write(XX, buf, n);
		}
		close(XX);
		wait(NULL);
	}
	else {
		close(XX);
		dup2(XX, XX);
		close(XX);
		execlp("sort", "sort", NULL);
		perror("exec");
		exit(1);
	}
	return(0);
}
