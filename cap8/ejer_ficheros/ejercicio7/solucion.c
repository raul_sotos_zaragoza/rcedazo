#include <dirent.h>
#include <stdio.h>

int main(int argc, char **argv){/* directorio se pasa como argumento */
	DIR *dirp;
	struct dirent *dp;
	
	if (argc!=2) {
                fprintf(stderr, "Uso: %s directorio\n", argv[0]);
                return 1;
        }

	dirp = opendir(argv[1]);
	if (dirp == NULL)  {
		fprintf(stderr,"No puedo abrir %s\n", argv[1]);
	} else {
		while ((dp = readdir(dirp)) != NULL)
			if (dp->d_name[0]!='.')
				printf("%s\n", dp->d_name);
		closedir(dirp);  
	}
	return 0;
}
