// De: "Apuntes de Sistemas Informáticos Industriales" Carlos Platero.
// (R) 2013 Con licencia GPL.
// Ver permisos en licencia de GPL

#ifndef _INC_MFCNOMBRE_
#define _INC_MFCNOMBRE_

#include <afx.h>
#include "INombre.h"
 

class MFCNombre : public INombre
{
private:
	CString elNombre;

public:
	virtual void setNombre(const char *punteroNombre)
		{elNombre = punteroNombre;}
	virtual const char * getNombre (void)
		{ return (elNombre);}



	

};

#endif 

