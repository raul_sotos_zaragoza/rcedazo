
/************************************************************************/
/*Proposito: Mostrar el funcionamiento de las clases parametrizadas		*/
/*Autor: CPD															*/
/*Fecha: 18/09/13															*/
/*Versi�n: 0.0															*/
/************************************************************************/

#include <iostream>
#include "../includes/ListaPasajeros.h"
using namespace std;




int main ( void )
{

	ListaPasajeros laListaPasajeros;

	cout<<"Introducir datos de los pasajeros"<<"\n";
	bool bSalir = false;

	while (!bSalir )
	{
		string nombre, apellido;
		Pasajero p1;
		unsigned long numPasaporte;

		cout<<"Nombre del pasajero: ";
		cin>> nombre;
		cout<<"\nApellido del pasajero: ";
		cin>> apellido;
		nombre += ' ' + apellido;

		cout<<"\n"<<"Numero Pasaporte: ";
		cin>> numPasaporte;

		p1.setPasaporte(numPasaporte);


		p1.setNombre( nombre.c_str() );
		laListaPasajeros.setDatosPasajero(p1);

		cout<<"\nIntroducir otro pasajero (s/n)? ";
		char letra;
		cin >> letra;
		bSalir = (letra == 's') || (letra == 'S')?false:true;
	}


	// Listar el pasaje
	cout<<"Lista completa de pasaje:\n";

	for(int i =0;i<laListaPasajeros.darNumeroPasajeros();i++)
	{

		Pasajero elPasajero;
		elPasajero = laListaPasajeros.getDatoPasajero(i);

		cout << "Pasajero : "<< elPasajero.getNombre();
		cout <<" ;Pasaporte : "<<elPasajero.getPasaporte() <<"\n";

	}
    return 0;

}
