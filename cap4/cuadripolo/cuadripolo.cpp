#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

typedef enum{R,L,C} TipoElemento;

class ElementoPasivo {
	TipoElemento tipo;
	float valor;
	unsigned identificador;
public:
	ElementoPasivo(TipoElemento t,float v,unsigned i): 
	  tipo(t), valor(v),identificador(i) {}
	void setValor(float v) {valor=v;}
	void setIdentificador(unsigned i) {identificador=i;}
	TipoElemento getTipo() { return tipo;}
	float getValor() {return valor;}
	unsigned getIdentificador() {return identificador;}
	friend ostream& operator<<(ostream& os, const ElementoPasivo * p) {
/*		if(p->tipo == R) return os << "Resistencia " << p->identificador 
			<< ": valor = " << p->valor << endl;
		else if(p->tipo == L) return os << "Bobina " << p->identificador 
			<< ": valor = " << p->valor << endl;
		else return os << "Condensador " << p->identificador 
			<< ": valor =" << p->valor << endl;*/
		return os << p->identificador  << ": valor = " << p->valor << endl;

    }
	static ElementoPasivo * metodoFabricacion(TipoElemento,float,unsigned);

};

class Resistencia: public ElementoPasivo {
	friend class ElementoPasivo;
	Resistencia(float v,unsigned i): ElementoPasivo(R,v,i) {}

public:
//	Resistencia(float v,unsigned i): ElementoPasivo(R,v,i) {}
	friend ostream& operator<<(ostream& os, const Resistencia * p) {
		return os << "Resistencia " << (ElementoPasivo *)p;
    }
};

class Bobina: public ElementoPasivo {
	friend class ElementoPasivo;
	Bobina(float v,unsigned i): ElementoPasivo(L,v,i) {}

public:
//	Bobina(float v,unsigned i): ElementoPasivo(L,v,i) {}
	friend ostream& operator<<(ostream& os, const Bobina * p) {
		return os << "Bobina " << (ElementoPasivo *)p;
    }

};

class Condensador: public ElementoPasivo {
	friend class ElementoPasivo;
	Condensador(float v,unsigned i): ElementoPasivo(C,v,i) {}

public:
//	Condensador(float v,unsigned i): ElementoPasivo(C,v,i) {}
	friend ostream& operator<<(ostream& os, const Condensador * p) {
		return os << "Bobina " << (ElementoPasivo *)p;
    }
};

void visualizar(ElementoPasivo *p){
	/*if(p->getTipo() == R) cout << "Resistencia " << p->getIdentificador() 
		<< ": valor = " << p->getValor() << endl;
	else if(p->getTipo() == L) cout << "Bobina " << p->getIdentificador() 
		<< ": valor = " << p->getValor() << endl;
	else cout << "Condensador " << p->getIdentificador() 
		<< ": valor =" << p->getValor() << endl;*/
//	cout << p;
	if(p->getTipo() == R) cout << (Resistencia *) p;
	else if(p->getTipo() == L) cout << (Bobina *) p;
	else cout << (Condensador *) p;

}



class Cuadripolo {
	unsigned numR;
	unsigned numL;
	unsigned numC;
	std::vector<ElementoPasivo *> listaComponentes;
public:
	Cuadripolo():numR(0),numL(0),numC(0){}
	~Cuadripolo(){
		for(unsigned i=0;i<listaComponentes.size();i++)
			delete listaComponentes[i];
	}
	void addComponent(TipoElemento t, float v){
		if(t==R)
//			listaComponentes.push_back(new Resistencia(v,++numR));
		listaComponentes.push_back(ElementoPasivo::metodoFabricacion(R,v,++numR));

		else if(t==L)
//			listaComponentes.push_back(new Bobina(v,++numL));
		listaComponentes.push_back(ElementoPasivo::metodoFabricacion(L,v,++numL));
		else
//			listaComponentes.push_back(new Condensador(v,++numC));
		listaComponentes.push_back(ElementoPasivo::metodoFabricacion(C,v,++numC));

	}



	void list(){
//		for(unsigned i=0;i<listaComponentes.size();i++)
//			cout << listaComponentes[i];
		for_each(listaComponentes.begin(),listaComponentes.end(),visualizar);

	}
};


ElementoPasivo * ElementoPasivo::metodoFabricacion(TipoElemento t,float v,unsigned num)
{
	if(t==R) return new Resistencia(v,num);
	else if (t==L) return new Bobina(v,num);
	else return new Condensador(v,num);
}

int main()
{
	Cuadripolo red;
	red.addComponent(R,68e3);
	red.addComponent(R,33e3);
	red.addComponent(R,27e3);
	red.addComponent(L,1e-3);
	red.addComponent(C,1e-8);
	red.addComponent(C,15e-8);
	red.list();
	return 0;

}
