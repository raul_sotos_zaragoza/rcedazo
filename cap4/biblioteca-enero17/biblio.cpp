#include <iostream>
#include <string>
#include <vector>
using namespace std;

typedef enum{LIBRO,REVISTA} TipoPublicacion;

class Publicacion{
protected:
	TipoPublicacion elTipo;
	string titulo;
public:
	static Publicacion * metodoFabricacion(TipoPublicacion,string,string,unsigned);
	virtual void listar() {
		cout << "Titulo: "<< titulo;
	}
};

class Libro : public Publicacion{
	string autor;
	friend class Publicacion;
	Libro(string &t,string &a):autor(a){ titulo=t; }
public:
	virtual void listar() {
		Publicacion::listar();
		cout<< " Autor: " << autor << endl;
	}

};

class Revista : public Publicacion{
	unsigned anyo;
	friend class Publicacion;
	Revista(string &t,unsigned a):anyo(a){ titulo=t; }
public:
	virtual void listar() {
		Publicacion::listar();
		cout<< " Fecha: " << anyo << endl;
	}

};

class Biblioteca {
	vector<Publicacion *> listado;
public:
	Biblioteca(){}
	~Biblioteca() {
		for(int i=0;i<listado.size();i++)
			delete listado[i];
	}

	void listar() {
		for(int i=0;i<listado.size();i++)
			listado[i]->listar();
	}

	void add_Publicacion(Publicacion *p) {
		listado.push_back(p);
	}
};

Publicacion * Publicacion::metodoFabricacion(TipoPublicacion elTipo,string t,string a,unsigned f=0)
{
	if(elTipo==LIBRO) return new Libro(t,a);
	else if(elTipo==REVISTA) return new Revista(t,f);
	else return 0;
}


int main()
{
	Biblioteca biblioteca;
	biblioteca.add_Publicacion(Publicacion::metodoFabricacion(LIBRO,"El Quijote","Cervantes"));
	biblioteca.add_Publicacion(Publicacion::metodoFabricacion(REVISTA,"Mundo Electronico","",2016));
	cout << "Lista de Publicaciones:" << endl;
	biblioteca.listar();
	return 0;
}

