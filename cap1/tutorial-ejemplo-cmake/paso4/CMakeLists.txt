cmake_minimum_required (VERSION 2.6)
project (Tutorial)

# Se añade un include_directory para que el fichero de encabezado MathFunctions/mysqrt.h sea encontraro para el prototipo de la función
include_directories ("${PROJECT_SOURCE_DIR}/MathFunctions")

# se añade una llamada add_subdirectory para hacer uso de la nueva librería
add_subdirectory (MathFunctions)

# añadimos el ejecutable
add_executable(Tutorial tutorial.cxx)
target_link_libraries (Tutorial MathFunctions)

# add the install targets
install (TARGETS Tutorial DESTINATION "${PROJECT_SOURCE_DIR}/bin")
#install (FILES "${PROJECT_BINARY_DIR}/TutorialConfig.h" 
#  DESTINATION "${PROJECT_SOURCE_DIR}/include")

# enable testing
enable_testing ()

# does the application run
add_test (TutorialRuns Tutorial 25)

# does it sqrt of 25
add_test (TutorialComp25 Tutorial 25)
set_tests_properties (TutorialComp25 
  PROPERTIES PASS_REGULAR_EXPRESSION "25 es 5"
  )

# does it handle negative numbers
add_test (TutorialNegative Tutorial -25)
set_tests_properties (TutorialNegative
  PROPERTIES PASS_REGULAR_EXPRESSION "-25 es 0"
  )

# does it handle small numbers
add_test (TutorialSmall Tutorial 0.0001)
set_tests_properties (TutorialSmall
  PROPERTIES PASS_REGULAR_EXPRESSION "0.0001 es 0.01"
  )

# does the usage message work?
add_test (TutorialUsage Tutorial)
set_tests_properties (TutorialUsage
  PROPERTIES 
  PASS_REGULAR_EXPRESSION "Uso:.*numero"
  )
