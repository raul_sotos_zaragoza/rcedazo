#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/syscall.h>

pthread_mutex_t mutex; /* Control de acceso */
pthread_cond_t a_leer, a_escribir; /* Condiciones de espera */
int leyendo, escribiendo; /* Variables de control: Estado del acceso */

void Lector(void)
{
    pthread_mutex_lock(&mutex);
    while(escribiendo != 0) /*condición espera */
        pthread_cond_wait(&a_leer, &mutex);
    leyendo++;
    pthread_mutex_unlock(&mutex);
    // ...
    pthread_mutex_lock(&mutex);
    leyendo--;
    if (leyendo == 0)
        pthread_cond_signal(&a_escribir);
    pthread_mutex_unlock(&mutex);
    pthread_exit(0);
}
void Escritor(void)
{
    pthread_mutex_lock(&mutex);
    while(leyendo !=0 || escribiendo !=0) /*condición espera */
        pthread_cond_wait(&a_escribir, &mutex);
    escribiendo++;
    pthread_mutex_unlock(&mutex);
    // ...
    pthread_mutex_lock(&mutex);
    escribiendo--;
    pthread_cond_signal(&a_escribir);
    pthread_cond_broadcast(&a_leer);
    pthread_mutex_unlock(&mutex);
    pthread_exit(0);
}

int main(void)
{
    pthread_t th1, th2, th3, th4;
    pthread_mutex_init(&mutex, NULL); /* Situación inicial */
    pthread_cond_init(&a_leer, NULL);
    pthread_cond_init(&a_escribir, NULL);
    pthread_create(&th1, NULL, Lector, NULL); /* Arranque */
    pthread_create(&th2, NULL, Escritor, NULL);
    pthread_create(&th3, NULL, Lector, NULL);
    pthread_create(&th4, NULL, Escritor, NULL);
    pthread_join(th1, NULL); /* Esperar terminación */
    pthread_join(th2, NULL);
    pthread_join(th3, NULL);
    pthread_join(th4, NULL);
    pthread_mutex_destroy(&mutex); /* Destruir */
    pthread_cond_destroy(&a_leer);
    pthread_cond_destroy(&a_escribir);
    return 0;
}
