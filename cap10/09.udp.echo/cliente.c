#include <sys/types.h>
#include <sys/socket.h>
#include <stdio.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <string.h>
#include <netdb.h>

int main(int argc, char *argv[])
{
  int sd, size;
  unsigned char byte;
  struct sockaddr_in server_addr, client_addr;
  struct hostent *hp;

  sd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
  if (sd < 0){
    printf("Error en socket\n");
    return 1;
  }

  // DIRECCION DEL SERVIDOR
  /* se obtiene y se rellena la dirección del servidor */
  bzero((char *)&server_addr, sizeof(server_addr));
  hp = gethostbyname (argv[1]); // dominio
  if (hp == NULL){
    printf("Error en la llamada gethostbyname\n");
    return 1;
  }
  memcpy (&(server_addr.sin_addr), hp->h_addr, hp->h_length);
  server_addr.sin_family = AF_INET;
  server_addr.sin_port = htons(4200);

  // Se hace el bind implícitamente por el send
  while (read(0, &byte, 1) == 1) {

	/* se envía la petición al servidor */
	sendto(sd, &byte, 1, 0, (struct sockaddr *)&server_addr, sizeof(server_addr));

	/* se recibe la respuesta */
	recvfrom(sd, &byte, sizeof(int), 0, (struct sockaddr *)&server_addr, &size);
        write (1, &byte, 1);
  }
  close (sd);
  return 0;
}


