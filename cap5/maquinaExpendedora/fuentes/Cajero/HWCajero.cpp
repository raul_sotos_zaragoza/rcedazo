// HWCajero.cpp: implementation of the HWCajero class.
//
//////////////////////////////////////////////////////////////////////
#include <iostream>
#include <iomanip>
#include "../../cabeceras/Cajero/HWCajero.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

HWCajero::HWCajero()
{

}

HWCajero::~HWCajero()
{

}

Moneda HWCajero::recibirMoneda()
{
	float cantidad;
	std::cin >> std::setprecision(2)>> cantidad;
	return(Moneda((unsigned)(cantidad*FACTOR_DINERO)));
}


Dinero HWCajero::solicitarProducto()
{
	std::cout<<"Elegir tipo producto:"<<std::endl;
	std::cout<<"====================="<<std::endl;
	std::cout<<"1: Cafe  40 centimos"<<std::endl;
	std::cout<<"2: Te    35 centimos"<<std::endl;
	std::cout<<"3: Limon 50 centimos"<<std::endl;
	unsigned opcion;unsigned valor;
	std::cin>>opcion;
	switch(opcion)
	{
	case 1: valor = 40;break;
	case 2: valor = 35;break;
	default: valor = 50;
	}
	std::cout<<"Introducir monedas:"<<std::endl;
	std::cout<<"==================="<<std::endl;

	return(Dinero(valor));
}



void HWCajero::entregarVueltas(std::vector<Moneda> &laListaMonedas)
{
	std::cout<<"Lista de monedas a devolver"<<std::endl;
	for(unsigned i=0;i<laListaMonedas.size();i++)
		std::cout<<((float)laListaMonedas[i].getCantidad())/FACTOR_DINERO
		<<std::setprecision(2)
		<<std::endl;
}

