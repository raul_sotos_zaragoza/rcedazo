// HWCajero.h: interface for the HWCajero class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_HWCAJERO_H__2BCE1CC9_CFE7_4BEB_9AEF_0E077E0F3237__INCLUDED_)
#define AFX_HWCAJERO_H__2BCE1CC9_CFE7_4BEB_9AEF_0E077E0F3237__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <vector>
#include "../comunes/Moneda.h"

class HWCajero
{
public:
	HWCajero();
	virtual ~HWCajero();

public:
	Moneda recibirMoneda();
	Dinero solicitarProducto();
	void entregarVueltas(std::vector<Moneda> &);


};

#endif // !defined(AFX_HWCAJERO_H__2BCE1CC9_CFE7_4BEB_9AEF_0E077E0F3237__INCLUDED_)
