// OrdenarNumeros.h: interface for the OrdenarNumeros class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ORDENARNUMEROS_H__AA14AB02_F328_4EC6_8AB2_724E4EC5C202__INCLUDED_)
#define AFX_ORDENARNUMEROS_H__AA14AB02_F328_4EC6_8AB2_724E4EC5C202__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <vector>
#include "../Visualizador/GUIOrdenarNumeros.h"

class OrdenarNumeros  
{
	GUIOrdenarNumeros elVisualizador;
	std::vector<double> elVectorNumeros;
public:
	OrdenarNumeros();
	virtual ~OrdenarNumeros();
	void introducirDatos();
	void visualizarResultados();

};

#endif // !defined(AFX_ORDENARNUMEROS_H__AA14AB02_F328_4EC6_8AB2_724E4EC5C202__INCLUDED_)
